import numpy as np
from scipy.signal import spectrogram
from scipy.fft import fftshift, set_workers, get_workers
from dataclasses import dataclass
from .linearization import get_linearization, linearize
from .utilities import closest
from .read_rtr_data import get_default_shotfile_dir, get_sampling_frequency, get_band_signal, get_timestamps
from .waveguide_dispersion import aug_tgcorr2, get_dispersion_phase
from .burst_mode_handler import burst_mode_handler



DEFAULT_SWEEP_LINEARIZATION = 24


def normalize(x, n=1, axis=-1):
    return x / np.max(np.abs(x), axis=axis) * n

@dataclass
class Band:
    name: str
    side: str
    dtype: str
    frequency: np.ndarray = 0
    amplitude: np.ndarray = 0
    nperseg: int = 0
    noverlap: int = 0
    nfft: int = 0
    x: np.ndarray = 0
    y: np.ndarray = 0
    spectrogram: np.ndarray = 0
    bg_spectrogram: np.ndarray = 0
    spectrogram_corrected: np.ndarray = 0
    x_sampled: np.ndarray = 0
    y_sampled: np.ndarray = 0
    spectrogram_sampled: np.ndarray = 0
    dispersion: np.ndarray = 0
        
    def __init__(self, name, side, dtype):
        self.name = name
        self.side = side
        self.dtype = dtype
        
        
        
def compute_spectrograms(band, sampling_frequency):
    
    # Compute spectrograms ----------------------------------------------------------------------
    # with set_workers(8):

    #     print(f"Number of workers: {get_workers()}")

    band.y, band.x, band.spectrogram = spectrogram(
        band.amplitude,
        fs=sampling_frequency,
        nperseg=band.nperseg, 
        noverlap=band.noverlap, 
        nfft=band.nfft,
        window='boxcar',
        detrend=False,
#         return_onesided=False,
#         mode='magnitude',
#         scaling='spectrum',
    )

    # Normalize spectrograms -------------------------------
#     band.spectrogram /= band.spectrogram.max(axis=0)

    # Remap the x-axis to frequency domain -----------------
    band.x = band.frequency[0] + band.x * (band.frequency[-1] - band.frequency[0]) / 25e-6 
    
    if band.amplitude.dtype == complex:
        band.y = fftshift(band.y)
        band.spectrogram = fftshift(band.spectrogram, axes=-2)
    
    return band


def get_band_signal_linearized(shot, shotfile_dir, name, side, dtype, burst, sweep, shot_linearization=None, linearization_shotfile_dir=None, sweep_linearization=None):
    """Get the reflectometry signals already linearized.
    
    Parameters
    ----------
        shot: int
            Shot number
        shotfile_dir: str
            Path to the shotfile
        name: one of {'K', 'Ka', 'Q', 'V', 'W'}
            Name of the band from which to get the signal
        side: one of {'HFS', 'LFS'}
            Side from which to get the signal
        dtype: {'real', 'complex'}
            Data type of the reflectometry signal
        burst: int
            Number of signals returned starting from sweep number *sweep*
        shot_linearization: int, optional
            Alternative shot number to server as linearization source
        linearization_shotfile_dir: str, optional
            Path to the alternative shotfile. Only necessary if the alternative shotfile is not in the default RTR directory in AFS
        sweep_linearization: int, optional
            Sweep number for the alternative linearization source. Defaults to the same value as *sweep*
            
    Returns
    -------
        frequency_linear: ndarray
            Uniformly spaced frequency samples
        signal: ndarray
            Corresponding signal amplitude
    
    """
    
    if shot_linearization is None:
        shot_linearization = shot
        linearization_shotfile_dir = shotfile_dir
        sweep_linearization = sweep
        
    else:
        if linearization_shotfile_dir is None:
            linearization_shotfile_dir = get_default_shotfile_dir(shot_linearization)

        if sweep_linearization is None:
            sweep_linearization = sweep
        
        
    # get the signal linearization curve -------------------------
    frequency = get_linearization(shot_linearization, sweep_linearization, name, shotfile_dir=linearization_shotfile_dir)

    signal = get_band_signal(shot, shotfile_dir, name, side, dtype, sweep, burst)
        
    return linearize(frequency, signal)


def batch_processing(shot, band_name, band_side, band_dtype, start_time, end_time, time_step, burst, lower_filter, upper_filter, subtract_background_on_bands, subtract_dispersion_on_bands, shotfile_dir=None, shot_linearization=None, linearization_shotfile_dir=None, sweep_linearization=None, nperseg=None, nfft=None, noverlap=None):
    

    # Check that *burst* is odd --------------------------------
    if burst % 2 == 0:
        raise ValueError("Burst must be an odd integer")
    
    # Get signals ----------------------------------------------
    if shotfile_dir is None:
        shotfile_dir = get_default_shotfile_dir(shot)

    if shot_linearization is None:
        shot_linearization = shot
        sweep_linearization = DEFAULT_SWEEP_LINEARIZATION
        linearization_shotfile_dir = shotfile_dir
        
    sampling_frequency = get_sampling_frequency(shot, shotfile_dir)
    
    timestamps = get_timestamps(shot, shotfile_dir)
    
    # Read the raw data -------------------------------------------
    
    # Create object to hold the data
    band = Band(band_name, band_side, band_dtype)
    bg_band = Band(band_name, band_side, band_dtype)
    
    # Get the frequency sweeping curve
    frequency = get_linearization(shot_linearization, sweep_linearization, band.name, shotfile_dir=linearization_shotfile_dir)
    
    # Call the burst mode handling routine
    all_sweeps, central_sweeps, burst_mode_kernel = burst_mode_handler(timestamps, start_time, end_time, time_step, burst)
    
    # Get all signals from the first to the last sample according to *sweeps* and *burst*
    signal = get_band_signal(shot, shotfile_dir, band.name, band.side, band.dtype, all_sweeps)
    
    # Linearize the signals ---------------------------------------
    # TODO: Vectorize or move to Cython
#     for i in range(len(signal)):
#         frequency_linear, signal[i] = linearize(frequency, signal[i])  

    # Batch linearization one to many ----------------------------
    frequency_linear, signal = linearize(frequency, signal) 

    # Save the signal -------------------------
    band.frequency = frequency_linear
    band.sweep_rate = (band.frequency[1] - band.frequency[0]) * sampling_frequency
    band.amplitude = signal

    if signal.dtype == complex:
        signal -= 2**11 + 1j * 2**11
    else:
        signal -= 2**11

    # Subtract dispersion to signal -------------------------
    if f"{band.name}-{band.side}" in subtract_dispersion_on_bands:
        correction = get_dispersion_phase(shot, band.name, band.side, band.frequency[0], band.sweep_rate, np.arange(signal.shape[-1])/sampling_frequency)
        signal *= correction
    
    # Make spectrograms --------------------------------------------
    band.nperseg = nperseg
    band.nfft = nfft
    band.noverlap = noverlap
    band = compute_spectrograms(band, sampling_frequency)
    band.burst_spectrogram = burst_mode_kernel(band.spectrogram)
    
    # Get background -----------------------------------------------
    
    # Get the first *burst* signals of the acquisition
    bg_signal = get_band_signal(shot, shotfile_dir, bg_band.name, bg_band.side, bg_band.dtype, 0, burst)
    if bg_signal.dtype == complex:
        bg_signal -= 2**11 + 1j * 2**11
    else:
        bg_signal -= 2**11
#     print(f"bg_signal.shape {bg_signal.shape}")
    
    # Linearize the background signals ---------------------------------------
    # TODO: Vectorize or move to Cython
    for i in range(len(bg_signal)):
        frequency_linear, bg_signal[i] = linearize(frequency, bg_signal[i])   

    # Save the background signal -------------------------
    bg_band.frequency = frequency_linear
    bg_band.sweep_rate = (bg_band.frequency[1] - bg_band.frequency[0]) * sampling_frequency
    bg_band.amplitude = bg_signal

    # Subtract dispersion to background signal -------------------------
    if f"{bg_band.name}-{bg_band.side}" in subtract_dispersion_on_bands:
        correction = get_dispersion_phase(shot, bg_band.name, bg_band.side, bg_band.frequency[0], bg_band.sweep_rate, np.arange(bg_band.amplitude.shape[-1])/sampling_frequency)
        bg_band.amplitude *= correction

    
    # Make background spectrograms --------------------------------------------
    bg_band.nperseg = nperseg
    bg_band.nfft = nfft
    bg_band.noverlap = noverlap
    bg_band = compute_spectrograms(bg_band, sampling_frequency)
    bg_band.burst_spectrogram = np.average(bg_band.spectrogram, axis=0)
    
    # Subtract background
    if f"{band.name}-{band.side}" in subtract_background_on_bands:
        print(f"Intensity of signal {np.sum(band.burst_spectrogram[0])}\nIntensity of background: {np.sum(bg_band.burst_spectrogram)}")
        band.burst_spectrogram = band.burst_spectrogram - bg_band.burst_spectrogram
    
    # Get dispersion ----------------------------------------------------------
    
    tau_offset = aug_tgcorr2(band.name, band.side, band.x * 1e-9, shot, ws='64')
    band.dispersion = tau_offset * (band.frequency[1] - band.frequency[0]) * sampling_frequency 

    # If dispersion was subrtacted, zero the dispersion -----------------------
    if f"{band.name}-{band.side}" in subtract_dispersion_on_bands:
        band.dispersion *= 0.
        tau_offset *= 0.
    
    # Filter spectrograms (zero the values above and bellow the filters)
    band.burst_spectrogram[np.broadcast_to(band.y[:, None], band.burst_spectrogram.shape) <= band.dispersion + lower_filter] = band.burst_spectrogram.min()
    band.burst_spectrogram[np.broadcast_to(band.y[:, None], band.burst_spectrogram.shape) >= band.dispersion + upper_filter] = band.burst_spectrogram.min()
    
    # Get group delay --------------------------------------------------------
    
    # By finding the maximum -----------------------
    
#     band.max_beat_frequency = band.y[np.argmax(band.burst_spectrogram, axis=1)]
#     band.tau = band.max_beat_frequency / band.sweep_rate
    
    # By quadratic fitting (better) ----------------
    
    # Indices of the maxima
    max_idx = np.argmax(band.burst_spectrogram, axis=1)
    
    # Indices of the neighbours, handle edge cases by reflection
    left_idx = max_idx - 1
    left_idx[left_idx == -1] = 1
    right_idx = max_idx + 1
    right_idx[right_idx == band.burst_spectrogram.shape[1]] = band.burst_spectrogram.shape[1] - 2

    # Get the max amplitude and its neighbours
    max_beat_amplitude = np.take_along_axis(band.burst_spectrogram, max_idx[:, None, :], axis=1)[:, 0, :]
    left_beat_amplitude = np.take_along_axis(band.burst_spectrogram, left_idx[:, None, :], axis=1)[:, 0, :]
    right_beat_amplitude = np.take_along_axis(band.burst_spectrogram, right_idx[:, None, :], axis=1)[:, 0, :]
    
    # Compute the peak frequency using a parabola ref. O. Smith Spectral Processing
    def quadratic_peak_finding(a, b, c):
        p = 0.5 * (a - c) / (a - 2 * b + c)
        y_p = b - 0.25 * (a - c) * p
        return p, y_p
        
    p, estimate_maximum_amplitude = quadratic_peak_finding(left_beat_amplitude, max_beat_amplitude, right_beat_amplitude)
    band.estimate_maximum_frequency = (max_idx + p) * sampling_frequency / band.nfft
    
    band.tau = band.estimate_maximum_frequency / band.sweep_rate

    
    
    
    # Subtract dispersion -----------------------------------------------------
    band.tau = band.tau - tau_offset
    
    # Return group delay ------------------------------------------------------
    return np.take(timestamps, central_sweeps), band.x, band.tau 
    