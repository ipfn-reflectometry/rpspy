def which_campaign(shot):
    """Return from which campaign is shot number *shot*.
    
    Parameters
    ----------
    shot: int
        Shot number.
        
    Returns
    -------
    campaign: str
        AUG campaign that contains *shot*, e.g., '2022' or '2015-16' 
    """
    if shot >= 39791: # 2022 (01.11.2021 - 31.08.2022)
        return "2022"
    elif shot >= 38202: # 2021 (01.11.2020 - 31.08.2021)
        return "2021"
    elif shot >= 37001: # 2020 (01.01.2020 - 31.08.2020)
        return "2020"
    elif shot >= 35989: # 2019 (01.04.2019 - 31.12.2019)
        return "2019"
    elif shot >= 34996: # 2018 (01.01.2018 - 31.03.2019)
        return "2018"
    elif shot >= 33725: # 2017 (01.01.2017 - 31.12.2017)
        return "2017"
    elif shot >= 31777: # 2015/16 (01.01.2015 - 01.07.2016)
        return "2015-16"
    elif shot >= 30150: # 2014 (01.01.2014 - 31.12.2014)
        return "2014"
    elif shot >= 27405: # 2012/13 (01.01.2012 - 01.07.2013)
        return "2012-13"
    elif shot >= 25903: # 2010/11 (01.01.2010 - 31.12.2011)
        return "2010-11"
    elif shot >= 24190: # 2009 (01.01.2009 - 31.12.2009)
        return "2009"
    elif shot >= 22586: # 2008 (01.01.2008 - 31.12.2008)
        return "2008"
    else:
        raise ValueError("Shot is too old. No records available")