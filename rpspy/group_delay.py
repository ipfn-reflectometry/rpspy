from scipy.integrate import simpson
import numpy as np
import scipy.constants as cst
from scipy.interpolate import griddata

def group_delay(r, ne, freq):
    """Calculate the group-delay under the WKB approximation for a given density profile
    
    Parameters
    ----------
    r: ndarray
        radial coordinates of the density profile. The simulation assumes the antenna is at r=0 and probes in the positive direction.
        
        *r* can have shape (N,), or (M, N,) to simulate M different density profiles.
        
        r is in meters.
        
    ne: ndarray
        Plasma density at each radial position specified by *r*. Units are m^-3.
        
        *ne* must have the same shape as *r*.
        
    freq: float, ndarray
        Probing frequencies at which to calculate the beat_frequency. Units are Hz.
        
    Returns
    -------
    group_delay: float, ndarray
        Float or ndarray the same size as *f* with the calculated group delay in seconds.
    """

    try:
#         f = np.linspace(freq.min() - 1e9, freq.max() + 1e9, 100000)
        f = np.linspace(freq.min(), freq.max(), 1000)
    except AttributeError:
        f = np.linspace(freq - 1e9, freq + 1e9, 1024)
    
    # Resample the density profile ---------------------------------------
    x = np.linspace(r[0], r[-1], 1000)
    y = griddata(r, ne, x)
    

    # Plasma frequency squared for each position -------------------------
    wp_2 = cst.e ** 2 * y / cst.epsilon_0 / cst.m_e
    
    # Radial frequency for convenience -----------------------------------
    w = (f * 2 * np.pi)
    w_2 = (f * 2 * np.pi) ** 2

    # Refractive index squared 
    # rows are frequency, cols are position ------------------------------
    n2 = 1 - wp_2 / (w[1:]**2)[:, None]
    
    # First row handmade because of divide-by-zero -----------------------
    n2 = np.concatenate((np.zeros((1, n2.shape[-1])), n2), axis=0)

    # Refractive index: clip the negative values and take square root ---
    n = np.clip(n2, a_min=0, a_max=np.inf)**0.5
    
    # PAY CLOSE ATTENTION
    # Need to clip not only the negative, but all values after the negative!
#     import numpy as np
#     test = np.array([[0, 2, 5, 4, 6, 4, 3, 1, 0], [0, 2, 7, 4, 6, 4, 3, 1, 0]])
#     args = np.argmax(test, axis=-1)
    # test[:, np.argmax(test, axis=-1):] = 0
#     if len(n.shape) == 2:

    # TODO: Probably does not work with higher dimensions
    mask_after_peak = np.indices(n.shape)[-1] >= np.argmin(n, axis=-1)[:, None]
    n[mask_after_peak] = 0.0
#     print(f"Tamanho: {n.shape}")

    # Phase of the returning wave ---------------------------------------
    phase = 2 * w / cst.c * simpson(n, x, axis=-1) - cst.pi / 2

    tau = np.gradient(phase, w)

    # Resample the result to the desired frequencies ----------------------------------------

    group_delay = np.interp(freq, f, tau)
    
    return group_delay


# Build a function
# def group_delay(position, density, frequency):
    
#     # Resample the density profile ---------------------------------------
#     x = np.linspace(position[0], position[-1], 1000)
#     y = griddata(position, density, x)
    
#     # Radial frequency for convenience -----------------------------------
#     f = np.linspace(frequency[0], frequency[-1], 2000)
#     w = f * 2 * cst.pi

#     # Plasma frequency squared for each position -------------------------
#     w_p2 = cst.e**2*y/cst.m_e/cst.epsilon_0

#     # Refractive index squared 
#     # rows are frequency, cols are position ------------------------------
#     n2 = 1 - w_p2 / (w[1:]**2)[:, None]
    
#     # First row handmade because of divide-by-zero -----------------------
#     n2 = np.concatenate((np.zeros((1, n2.shape[-1])), n2), axis=0)

#     # Refractive index: clip the negative values and take square root ---
#     n = np.clip(n2, a_min=0, a_max=np.inf)**0.5

#     # Phase of the returning wave ---------------------------------------
#     phase = 2*w/cst.c*simpson(n, x, axis=-1) - cst.pi/2
    
#     # Group delay -------------------------------------------------------
#     gd = np.gradient(phase)/np.average(np.diff(w))
    
#     # Resample and return -----------------------------------------------
#     return griddata(f, gd, frequency)