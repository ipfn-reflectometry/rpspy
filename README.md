# rpspy

Python scripts and functions of the O-mode reflectometry analysis toolchain

# Usage

To learn about the recontruction procedure use the notebook in the examples folder

To run the reconstruction as a script, use the script in the scripts folder

```cmd
/~> python shot_reconstruction.py --help
18:59:18 | aug_sfutils | INFO: Using version 0.8.0
18:59:18 | aug_sfutils.ww | INFO: WW loaded
18:59:18 | aug_sfutils.sfh | INFO: SFH loaded
18:59:19 | aug_sfutils.mapeq | INFO: AUG-SF home /shares/software/aug-dv/moduledata/aug_sfutils/0.8.0/aug_sfutils
Usage: shot_reconstruction.py [OPTIONS] SHOT

  Semi automated script to perform the full profile reconstruction. Results
  are written to a dump file with Jorge's architecture.

  Parameters ---------- shot: int     Shot number

Arguments:
  SHOT  [required]

Options:
  --destination-dir TEXT  [default: .]
  --start-time FLOAT      [default: 0.0]
  --end-time FLOAT        [default: 10.0]
  --time-step FLOAT       [default: 0.001]
  --burst INTEGER         [default: 29]
  --help                  Show this message and exit.
  ```