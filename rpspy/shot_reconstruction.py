import os
import numpy as np
from scipy.interpolate import griddata, interp1d
from .linearization import get_linearization_reference
from .profile_inversion import profile_inversion, f_to_ne
from .read_rtr_data import get_sampling_frequency, get_default_shotfile_dir
from .waveguide_dispersion import get_antenna_position, get_band_cutoff_frequency, get_A_B, get_reflectometry_limiter_position
from .dump import write_dump as _write_dump
from .write_shotfile import write_shotfile

import scipy.constants as cst
from rpspy.reconstruction import  Band, batch_processing

    

def full_profile_reconstruction(
    shot: int, 
    destination_dir: str = '.', 
    shotfile_dir=None, 
    linearization_shotfile_dir=None, 
    sweep_linearization=None, 
    shot_linearization=None,
    spectrogram_options=None, 
    filters=None,
    exclusion_regions=None,
    subtract_background_on_bands=None,
    subtract_dispersion_on_bands=None,
    start_time: float = 0.0, 
    end_time: float = 10.0, 
    time_step: float = 1e-3,
    burst: int = 29, 
    write_dump: bool = True, 
    write_private_shotfile: bool = False,
    write_public_shotfile: bool = False,
    return_profiles: bool = False
):
    """
    Performs full profile reconstruction for a given shot, with results optionally saved to a dump file.
    This process involves analyzing spectrograms, applying filters, and reconstructing
    density profiles over a specified time range.

    Parameters
    ----------
    shot : int
        Shot number identifying the experiment or simulation run.
    destination_dir : str, optional
        Directory to save the output dump file. Default is the current directory ('.').
    shotfile_dir : str, optional
        Directory containing the shot files. Default is None, indicating use of default location.
    linearization_shotfile_dir : str, optional
        Directory for linearization shot files. Default is None, indicating use of default location.
    sweep_linearization : int, optional
        Sweep number for linearization. Default is obtained internally.
    shot_linearization : int, optional
        Shot number for shot linearization. Default is obtained internally.
    spectrogram_options : dict, optional
        Configuration options for generating spectrograms. Default is:
        {
            'HFS': {
                'K': {'nperseg': 128, 'noverlap':96, 'nfft': 1024},
                'Ka': {'nperseg': 128, 'noverlap':96, 'nfft': 1024},
                'Q': {'nperseg': 128, 'noverlap':96, 'nfft': 1024},
                'V': {'nperseg': 32, 'noverlap':24, 'nfft': 1024},
            },
            'LFS': {
                'K': {'nperseg': 128, 'noverlap':96, 'nfft': 1024},
                'Ka': {'nperseg': 128, 'noverlap':96, 'nfft': 1024},
                'Q': {'nperseg': 128, 'noverlap':96, 'nfft': 1024},
                'V': {'nperseg': 32, 'noverlap':24, 'nfft': 1024},
            },
        }
    filters : dict, optional
        Dictionary of filters to apply during reconstruction. Default is:
        {
            'HFS': {
                'K': [0.0e6, 4.0e6],
                'Ka': [0.0e6, 4.0e6],
                'Q': [0.0e6, 4.0e6],
                'V': [0.0e6, 6.0e6],
            },
            'LFS': {
                'K': [0.2e6, 4.0e6],
                'Ka': [0.0e6, 4.0e6],
                'Q': [0.0e6, 4.0e6],
                'V': [0.8e6, 6.0e6],
            },
        }.
    exclusion_regions : dict, optional
        Dictionary of frequency exclusion regions in Hz. Default is None.
        Example:
        {
            'HFS': [
                [36.25e9, 38.5e9],
                [47.5e9, 61e9],
                [70.0e9, 100e9],
            ],
            'LFS': [
                [47e9, 58e9],
            ],
        }
    subtract_background_on_bands: list, optional
        List of '<band>-<side>' to subtract background. Example ['Q-LFS', 'Q-HFS']. 
    subtract_dispersion_on_bands: list, optional
        List of '<band>-<side>' to subtract background. Example ['Q-LFS', 'Q-HFS']. 
    start_time : float, optional
        Start time of the reconstruction in seconds. Default is 0.0.
    end_time : float, optional
        End time of the reconstruction in seconds. Default is 10.0.
    time_step : float, optional
        Time step for the reconstruction in seconds. Default is 1e-3.
    burst : int, optional
        Burst parameter for controlling the reconstruction process. Default is 29.
    write_dump : bool, optional
        If True, writes the reconstructed data to a dump file using the specified architecture. Default is True.
    write_private_shotfile : bool, optional
        If True, writes the reconstructed data to a private shotfile under the current user. Default is False.
    write_public_shotfile : bool, optional
        If True, writes the reconstructed data to a public shotfile. Default is False.
    return_profiles : bool, optional
        If True, returns the processed density profiles. Default is False.

    Returns
    -------
    tuple or None
        If `return_profiles` is True, returns a tuple containing:
        
        - time (ndarray): Time instants corresponding to the processed density profiles.
        - density (ndarray): Plasma density profile.
        - r_hfs (ndarray): Radial position of each density layer on the high-field side.
        - r_lfs (ndarray): Radial position of each density layer on the low-field side.
        
        If `return_profiles` is False, returns None.
    """

    
    
    # General processing parameters
    if shotfile_dir is None:
        shotfile_dir = get_default_shotfile_dir(shot)
    
    if not os.path.exists(shotfile_dir):
        raise ValueError(f"Raw data for shot {shot} was not found.") 
    

    sampling_frequency = get_sampling_frequency(shot, shotfile_dir)

    # Get a reference shot and sweep number to get the linearization from
    if sweep_linearization is None:
        _, sweep_linearization = get_linearization_reference(shot)
        
    if shot_linearization is None:
        shot_linearization, _ = get_linearization_reference(shot)
    
    if linearization_shotfile_dir is None:
        linearization_shotfile_dir = get_default_shotfile_dir(shot_linearization)


    # Define inner and outer limiter positions for initialization -------------------------------------
    inner_limiter = get_reflectometry_limiter_position('hfs')
    outer_limiter = get_reflectometry_limiter_position('lfs')

    hfs_gd_zero = 2 * (inner_limiter - get_antenna_position('hfs')) / cst.c
    lfs_gd_zero = 2 * (get_antenna_position('lfs') - outer_limiter) / cst.c

    # Use these bands
    used_bands = [
        ('K', 'HFS', 'real'), 
        ('Ka', 'HFS', 'real'), 
        ('Q', 'HFS', 'real'), 
        ('V', 'HFS', 'complex'),
        ('K', 'LFS', 'real'), 
        ('Ka', 'LFS', 'real'), 
        ('Q', 'LFS', 'real'), 
        ('V', 'LFS', 'complex')
    ]
    
    # Default spectrogram options
    if spectrogram_options is None:
        spectrogram_options = {}
        spectrogram_options['HFS'] = {
            'K': {'nperseg': 128, 'noverlap':96, 'nfft': 1024},
            'Ka': {'nperseg': 128, 'noverlap':96, 'nfft': 1024},
            'Q': {'nperseg': 128, 'noverlap':96, 'nfft': 1024},
            'V': {'nperseg': 32, 'noverlap':24, 'nfft': 1024},
        }
        spectrogram_options['LFS'] = {
            'K': {'nperseg': 128, 'noverlap':96, 'nfft': 1024},
            'Ka': {'nperseg': 128, 'noverlap':96, 'nfft': 1024},
            'Q': {'nperseg': 128, 'noverlap':96, 'nfft': 1024},
            'V': {'nperseg': 32, 'noverlap':24, 'nfft': 1024},
        }

    # Apply filters a la Jorge ----------------------
    if filters is None:
        filters = {
            'HFS': {
                'K': [0.0e6, 4.0e6],
                'Ka': [0.0e6, 4.0e6],
                'Q': [0.0e6, 4.0e6],
                'V': [0.0e6, 6.0e6],
            },
            'LFS': {
                'K': [0.2e6, 4.0e6],
                'Ka': [0.0e6, 4.0e6],
                'Q': [0.0e6, 4.0e6],
                'V': [0.8e6, 6.0e6],
            },
        }
    



    # Subtract background on these bands
    if subtract_background_on_bands is None:
        subtract_background_on_bands = ['Q-HFS', 'Q-LFS']

    # Read the raw data -------------------------------------------
    bands = [Band(name, side, dtype) for name, side, dtype in used_bands]

    for band in bands:
        band.upper_filter = filters[band.side][band.name][1]
        band.lower_filter = filters[band.side][band.name][0]
        band.nperseg = spectrogram_options[band.side][band.name]['nperseg']
        band.noverlap = spectrogram_options[band.side][band.name]['noverlap']
        band.nfft = spectrogram_options[band.side][band.name]['nfft']

    for band in bands:

        times, band.freq, band.gd = batch_processing(shot, band.name, band.side, band.dtype, start_time, end_time, time_step, burst, band.lower_filter, band.upper_filter, subtract_background_on_bands, subtract_dispersion_on_bands, shotfile_dir, shot_linearization, linearization_shotfile_dir, sweep_linearization, nperseg=band.nperseg, nfft=band.nfft, noverlap=band.noverlap)

    # -----------------------------------
    # MERGE TOGETHER ALL BANDS
    # -----------------------------------

    hfs_first_freq = np.inf
    lfs_first_freq = np.inf

    hfs_last_freq = -np.inf
    lfs_last_freq = -np.inf

    for band in bands:
        if band.side == 'HFS':
            hfs_first_freq = min(band.freq[0], hfs_first_freq)
            hfs_last_freq = max(band.freq[-1], hfs_last_freq)
        elif band.side == 'LFS':
            lfs_first_freq = min(band.freq[0], lfs_first_freq)
            lfs_last_freq = max(band.freq[-1], lfs_last_freq)

    hfs_all_freq = np.concatenate([band.freq for band in bands if band.side == 'HFS'])
    lfs_all_freq = np.concatenate([band.freq for band in bands if band.side == 'LFS'])

    hfs_all_gd = np.concatenate([band.gd for band in bands if band.side == 'HFS'], axis=1)
    lfs_all_gd = np.concatenate([band.gd for band in bands if band.side == 'LFS'], axis=1)

    # Filter out certain frequencies with exclusion regions -------------------------------------------

    print(times.shape)
    print(hfs_all_freq.shape)
    print(hfs_all_gd.shape)

    for l, u in exclusion_regions['HFS']:
        hfs_all_gd = hfs_all_gd[:, (hfs_all_freq < l) | (hfs_all_freq > u)]
        hfs_all_freq = hfs_all_freq[(hfs_all_freq < l) | (hfs_all_freq > u)]

    for l, u in exclusion_regions['LFS']:
        lfs_all_gd = lfs_all_gd[:, (lfs_all_freq < l) | (lfs_all_freq > u)]
        lfs_all_freq = lfs_all_freq[(lfs_all_freq < l) | (lfs_all_freq > u)]

    hfs_resampled_freq = np.linspace(hfs_first_freq*1.0001, hfs_last_freq*0.9999, 1000)
    lfs_resampled_freq = np.linspace(lfs_first_freq*1.0001, lfs_last_freq*0.9999, 1000)

    hfs_resampled_gd = interp1d(
        x=hfs_all_freq,
        y=hfs_all_gd,
        kind='linear',
        fill_value=np.nan,
        axis=-1,
    )(hfs_resampled_freq)

    print(times.shape)
    print(hfs_resampled_freq.shape)
    print(hfs_resampled_gd.shape)

    lfs_resampled_gd = interp1d(
        x=lfs_all_freq,
        y=lfs_all_gd,
        kind='linear',
        fill_value=np.nan,
        axis=-1,
    )(lfs_resampled_freq)

    # ----------------------------------------------------------
    # INITIALIZE AT THE LIMITER
    # ----------------------------------------------------------

    def linear_initialization(fp, gd, gd_at_zero, up_to):

        n_points = 20  # Number of points in the initialization

        idx = fp < up_to  # Only use first points for linear initialization

        xi = np.sum(fp[idx], axis=-1)
        xi_xi = np.sum(fp[idx] * fp[idx], axis=-1)
        xi_yi = np.sum(gd[:, idx] * fp[idx], axis=-1)

        slope = (xi_yi - gd_at_zero * xi) / xi_xi


        fp_extrapolated = np.append(np.linspace(0, fp[0], n_points)[:-1], fp)
        gd_extrapolated = np.append(slope[:, None] * fp_extrapolated[:n_points - 1] + gd_at_zero, gd, axis=-1)

        return fp_extrapolated, gd_extrapolated


    fp_hfs, gd_hfs = linear_initialization(hfs_resampled_freq, hfs_resampled_gd, hfs_gd_zero, up_to=19.5e9)

    fp_lfs, gd_lfs = linear_initialization(lfs_resampled_freq, lfs_resampled_gd, lfs_gd_zero, up_to=19.5e9)

    # -------------------------------------------------------------
    # PROFILE INVERSION
    # -------------------------------------------------------------

    hfs_antenna = get_antenna_position('hfs')
    lfs_antenna = get_antenna_position('lfs')

    # Resample frequency and group delay to 0.5 GHz spacing
    # f_hfs = np.arange(0, fp_hfs.max(), 0.5e9)
    f_hfs = np.linspace(0, 75e9, 151)
    gds_hfs = np.array([griddata(fp_hfs, gd, f_hfs) for gd in gd_hfs])

    # f_lfs = np.arange(0, fp_lfs.max(), 0.5e9)
    f_lfs = np.linspace(0, 75e9, 151)
    gds_lfs = np.array([griddata(fp_lfs, gd, f_lfs) for gd in gd_lfs])

    # Compute the abel inverse
    # r_hfs = np.array([profile_inversion(f_hfs, gd) for gd in gds_hfs])
    # r_hfs = r_hfs + hfs_antenna

    # r_lfs = np.array([profile_inversion(f_lfs, gd) for gd in gds_lfs])
    # r_lfs =  - r_lfs + lfs_antenna

    # With Dirk's help :)
    r_hfs = profile_inversion(f_hfs, np.clip(gds_hfs.T - hfs_gd_zero, a_min=0, a_max=np.inf), pwld_batch=True).T
    r_hfs = r_hfs + inner_limiter

    r_lfs = profile_inversion(f_lfs, np.clip(gds_lfs.T - lfs_gd_zero, a_min=0, a_max=np.inf), pwld_batch=True).T
    r_lfs =  - r_lfs + outer_limiter

    # ------------------------------------------------------------
    # Write dump file
    # ------------------------------------------------------------

    def t_to_s(time):
        """ Time to string, e.g., 3.5 -> '3s5' """
        return f"{time:.1f}".replace('.', 's')
    
    if not os.path.exists(destination_dir):
        try:
            os.makedirs(destination_dir)
        except OSError as e:
            # Handle any errors that may occur during directory creation
            raise Exception(f"Error creating directory '{destination_dir}': {e}")

    if write_dump:
        print(f"Writing dump file to {destination_dir}/{t_to_s(times[0])}_{t_to_s(times[-1])}.{shot}")
        _write_dump(times, f_hfs, f_to_ne(f_hfs), r_hfs, r_lfs, gds_hfs, gds_lfs, f"{destination_dir}/{t_to_s(times[0])}_{t_to_s(times[-1])}.{shot}")
        print("Dump file written.")


    if write_private_shotfile or write_public_shotfile:
        # Get important data to write shotfile
        group_delay_calibration = { \
            side: {
                band: {
                    'A': get_A_B(shot, band, side)[0], 'B': get_A_B(shot, band, side)[1], 'fc': get_band_cutoff_frequency(band) \
                } for band in filters[side] \
            } for side in filters
        }
        # Copy the filters dictionary for later use normalized to Nyquist frequency
        normalized_filters = {side: {band: [f / (sampling_frequency / 2) for f in filters[side][band]] for band in filters[side]} for side in filters}

    if write_private_shotfile:
        print(f"Writing private shotfile for shot {shot}")
        write_shotfile(
            time=times, 
            density=f_to_ne(f_hfs), 
            lfs_area=r_lfs, 
            hfs_area=r_hfs, 
            lfs_gd=gds_lfs, 
            hfs_gd=gds_hfs, 
            filters=normalized_filters,
            spectrogram_parameters=spectrogram_options,
            group_delay_calibration=group_delay_calibration,
            exclusion_regions=exclusion_regions,
            shot=shot,
            type='private',
        )
        print("Private shotfile written.")

    if write_public_shotfile:
        print(f"Writing public shotfile for shot {shot}")
        write_shotfile(
            time=times, 
            density=f_to_ne(f_hfs), 
            lfs_area=r_lfs, 
            hfs_area=r_hfs, 
            lfs_gd=gds_lfs, 
            hfs_gd=gds_hfs, 
            filters=normalized_filters,
            spectrogram_parameters=spectrogram_options,
            group_delay_calibration=group_delay_calibration,
            exclusion_regions=exclusion_regions,
            shot=shot,
            type='public',
        )
        print("Public shotfile written.")

    if return_profiles:
        return times, f_to_ne(f_hfs), r_hfs, r_lfs
    

    
    
if __name__ == '__main__':
    import typer
    typer.run(full_profile_reconstruction)
#     full_profile_reconstruction(int(sys.argv[1]), float(sys.argv[2]), float(sys.argv[3]))


# DOCSTRINGS FOR REFERENCE

# def write_shotfile(time, density, lfs_area, hfs_area, lfs_gd, hfs_gd, filters, spectrogram_parameters, group_delay_calibration, exclusion_regions, shot, type='private'):
    """Write the reflectometry data to a shotfile under experiment REFG.

    Parameters
    ----------
    time : numpy.ndarray (T,)
        Time points of the reflectometry data in units of s.
    density : numpy.ndarray (D,)
        Plasma density axis sampled on 151 points in units of m^-3.
    lfs_area : numpy.ndarray (T, D,)
        2D array of position as function of time and frequency/density for the LFS reflectometer.
    hfs_area : numpy.ndarray (T, D,)
        2D array of position as function of time and frequency/density for the HFS reflectometer.
    lfs_gd : numpy.ndarray (T, D,)
        Group delay as function of time and frequency/density for the LFS reflectometer.
    hfs_gd : numpy.ndarray (T, D,)
        Group delay as function of time and frequency/density for the HFS reflectometer.
    filters : dict
        Dictionary containing the filter parameters for the shotfile according to the following template.
        Filters values should counted from the dispersion line and normalized to the Nyquist frequency, i.e., [0, 1].
            filters = {
                "HFS": {
                    "K": [
                        (HFS_K_lower_filter - dispersion) / Nyquist_frequency,
                        (HFS_K_upper_filter - dispersion) / Nyquist_frequency
                    ],
                    "Ka": [
                        (HFS_Ka_lower_filter - dispersion) / Nyquist_frequency,
                        (HFS_Ka_upper_filter - dispersion) / Nyquist_frequency
                    ],
                    "Q": [
                        (HFS_Q_lower_filter - dispersion) / Nyquist_frequency,
                        (HFS_Q_upper_filter - dispersion) / Nyquist_frequency
                    ],
                    "V": [
                        (HFS_V_lower_filter - dispersion) / Nyquist_frequency,
                        (HFS_V_upper_filter - dispersion) / Nyquist_frequency
                    ]
                },
                "LFS": {
                    "K": [
                        (LFS_K_lower_filter - dispersion) / Nyquist_frequency,
                        (LFS_K_upper_filter - dispersion) / Nyquist_frequency
                    ],
                    "Ka": [
                        (LFS_Ka_lower_filter - dispersion) / Nyquist_frequency,
                        (LFS_Ka_upper_filter - dispersion) / Nyquist_frequency
                    ],
                    "Q": [
                        (LFS_Q_lower_filter - dispersion) / Nyquist_frequency,
                        (LFS_Q_upper_filter - dispersion) / Nyquist_frequency
                    ],
                    "V": [
                        (LFS_V_lower_filter - dispersion) / Nyquist_frequency,
                        (LFS_V_upper_filter - dispersion) / Nyquist_frequency
                    ]
                }
            }
        spectrogram_parameters : dict
            Dictionary containing the parameters for the spectrogram calculation according to the following template.
            spectrogram_parameters = {
                "HFS": {
                    "K": {
                        "nperseg": HFS K-band DFT window size,
                        "noverlap": HFS K-band DFT window overlap,
                    },
                    "Ka": {
                        "nperseg": HFS Ka-band DFT window size,
                        "noverlap": HFS Ka-band DFT window overlap,
                    },
                    "Q": {
                        "nperseg": HFS Q-band DFT window size,
                        "noverlap": HFS Q-band DFT window overlap,
                    },
                    "V": {
                        "nperseg": HFS V-band DFT window size,
                        "noverlap": HFS V-band DFT window overlap,
                    }
                },
                "LFS": {
                    "K": {
                        "nperseg": LFS K-band DFT window size,
                        "noverlap": LFS K-band DFT window overlap,
                    },
                    "Ka": {
                        "nperseg": LFS Ka-band DFT window size,
                        "noverlap": LFS Ka-band DFT window overlap,
                    },
                    "Q": {
                        "nperseg": LFS Q-band DFT window size,
                        "noverlap": LFS Q-band DFT window overlap,
                    },
                    "V": {
                        "nperseg": LFS V-band DFT window size,
                        "noverlap": LFS V-band DFT window overlap,
                    }
                }
            }
        group_delay_calibration : dict
            Dictionary containing the group delay calibration parameters according to the following template.
            Calibration equation is: group_delay_zero = A + B * sqrt(1-(f/fc) ** 2),
            where f is the frequency [Hz], fc is the cutoff frequency [Hz], and A and B are calibration constants [s].
            
            group_delay_calibration = {
                "HFS": {
                    "K": {
                        "A": HFS K-band calibration 'A' constant,
                        "B": HFS K-band calibration 'B' constant,
                        "fc": HFS K-band calibration 'fc' constant,
                    },
                    "Ka": {
                        "A": HFS Ka-band calibration 'A' constant,
                        "B": HFS Ka-band calibration 'B' constant,
                        "fc": HFS Ka-band calibration 'fc' constant,
                    },
                    "Q": {
                        "A": HFS Q-band calibration 'A' constant,
                        "B": HFS Q-band calibration 'B' constant,
                        "fc": HFS Q-band calibration 'fc' constant,
                    },
                    "V": {
                        "A": HFS V-band calibration 'A' constant,
                        "B": HFS V-band calibration 'B' constant,
                        "fc": HFS V-band calibration 'fc' constant,
                    }
                },
                "LFS": {
                    "K": {
                        "A": LFS K-band calibration 'A' constant,
                        "B": LFS K-band calibration 'B' constant,
                        "fc": LFS K-band calibration 'fc' constant,
                    },
                    "Ka": {
                        "A": LFS Ka-band calibration 'A' constant,
                        "B": LFS Ka-band calibration 'B' constant,
                        "fc": LFS Ka-band calibration 'fc' constant,
                    },
                    "Q": {
                        "A": LFS Q-band calibration 'A' constant,
                        "B": LFS Q-band calibration 'B' constant,
                        "fc": LFS Q-band calibration 'fc' constant,
                    },
                    "V": {
                        "A": LFS V-band calibration 'A' constant,
                        "B": LFS V-band calibration 'B' constant,
                        "fc": LFS V-band calibration 'fc' constant,
                    }
                }
            }
        exclusion_regions : dict
            Dictionary containing the exclusion regions for the shotfile according to the following template.
            exclusion_regions = {
                "HFS": [
                    [exclusion_region_1_start, exclusion_region_1_end],
                    [exclusion_region_2_start, exclusion_region_2_end],
                    ...
                ]
                "LFS": [
                    [exclusion_region_1_start, exclusion_region_1_end],
                    [exclusion_region_2_start, exclusion_region_2_end],
                    ...
                ]
            }
        shot : int
            Shot number of the reflectometry data.        
        type : str
            Type of the shotfile. Options are "private" or "public". Default is 'private'.
            If private, will write the shotfile under the current user private shotfile directory.
            If public, will write the shotfile under the AUGD experiment directory.
    
    """