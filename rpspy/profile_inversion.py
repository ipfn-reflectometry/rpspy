import scipy.constants as cst
from scipy.interpolate import interp1d
from scipy.integrate import simpson
import numpy as np
from .abel_stand_alone import PWLD_matrix_batch

# ne = conversion x fp^2
conversion = 4 * np.pi**2 * cst.m_e * cst.epsilon_0 / cst.e**2


def f_to_ne(f):
    return conversion * f**2


def ne_to_f(ne):
    return np.sqrt(ne / conversion)


# def profile_inversion(fp, gd):
#     # ne = conversion x fp^2
#     conversion = 4 * np.pi**2 * cst.m_e * cst.epsilon_0 / cst.e**2

#     # Build the gd array
#     resolution = 1024  # Choose resolution for integration purposes
#     f_probe_sampled = np.linspace(fp[0], fp[-1], resolution)
#     x_sampled = np.linspace(0.0, np.pi/2.0, resolution)
#     dx = np.pi / 2.0 / (resolution - 1)

#     integrand_sampled = f_probe_sampled[None,:].T * np.sin(x_sampled)[None, :]
#     gd_interpolator = interp1d(fp, gd, assume_sorted=True)
#     integrand_sampled = gd_interpolator(integrand_sampled)


#     radius = cst.c/np.pi * simpson(integrand_sampled, dx=dx)
    
#     return f_probe_sampled, radius


def profile_inversion(frequency, group_delay, resolution=1024, return_all=False, pwld_batch=False):
    """Profile inversion based on group delay data.
    This function implements both the Abel inversion and the piecewise linear density (PWLD) inversion.
    If the Abel inversion is used, the group delay is resampled using linear interpolation for integration accuracy.
    If the PWLD is used, no resampling is needed as the method implicitly assumes a linear density between measurements.
    
    Parameters
    ----------
    frequency: ndarray
        1D (M,) array of sampled frequencies in Hz units.
    group_delay: ndarray
        1D (M,) or 2D (N, M,) array of group delay measurements in seconds. If 2D, each row should correspond to one profile.
    resolution: int, optional
        Number of points used to sample frequency and group delay. Optional, defaults to 1024. Produces effects only if pwld_batch=False.
    return_all: bool, optional
        If True, will return a 3-tuple containing frequency, group delay, and position. If False, will return only position. Optional, defaults to False.
    pwld_batch: bool, optional
        Use the PWLD method. This method is particularly useful to quickly reconstruct a batch of density profiles by providing a 2D array of group delay values. Each row is assumed to be one profile. Optional, defaults to False.
    
        
    Returns
    -------
    f_sampled: ndarray
        Resampled frequency.
    gd_sampled: ndarray
        Resampled group delay.
    radius: ndarray
        Reflection distance in meters.


    Note: To maximize performance, choose pwld_batch=True and provide a 2D array of group delay measurements to simultaneously invert a batch of density profiles.
    """
    # PWLD BATCH MODE -------------------------------------
    
    if pwld_batch:
        gd_dim = len(group_delay.shape)
        if gd_dim == 1:
            gd = group_delay[:, None]
        else:
            gd = group_delay
        res = PWLD_matrix_batch(gd, frequency)
        res['R'] = np.insert(res['R'], 0, 0, axis=0)
        res['taugs'] = np.insert(res['taugs'], 0, 0, axis=0)
        
        if gd_dim == 1:
            res['R'] = res['R'][:,0]
            res['taugs'] = res['taugs'][:,0]
        
        if return_all:
            return res['f_P'], res['taugs'], res['R']

        return res['R']
    
    # END OF PWLD BATCH MODE -------------------------------
    

    # Resample the frequency and group delay axis
    f_sampled = np.linspace(frequency[0], frequency[-1], resolution)
    x_sampled = np.linspace(0.0, np.pi/2.0, resolution)
    dx = np.pi / 2.0 / (resolution - 1)
    
    # Compute integrand points
    integrand_sampled = f_sampled[None,:].T * np.sin(x_sampled)[None, :]
    gd_interpolator = interp1d(frequency, group_delay, fill_value='extrapolate', kind='linear')
    integrand_sampled = gd_interpolator(integrand_sampled)
    
    # Compute integral using simpsonon's rule
    radius = cst.c/np.pi * simpson(integrand_sampled, dx=dx)
    
    # Resample the result to match output
    radius_sampled = interp1d(f_sampled, radius, fill_value='extrapolate', kind='linear')(frequency)
    
    if return_all:
        # Resample the group delay for returning purposes
        gd_sampled = gd_interpolator(f_sampled)

        # Return up sampled results
        return f_sampled, gd_sampled, radius_sampled

    # Return result matching the input
    return radius_sampled