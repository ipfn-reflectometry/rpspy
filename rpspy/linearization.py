import os
import numpy as np
from scipy.signal import find_peaks, spectrogram
from scipy.interpolate import interp1d, CubicSpline
from scipy.integrate import cumulative_trapezoid
from dataclasses import dataclass
from .read_rtr_data import get_sampling_frequency, get_default_shotfile_dir


band_settings = {
    'K': {'oscilator_limits': np.array([8.500, 12.70]) * 1e9, 'multiplier': 2, 'calibration_nperseg': 40, 'peak_distance': 50},
    'Ka': {'oscilator_limits': np.array([12.50, 18.50]) * 1e9, 'multiplier': 2, 'calibration_nperseg': 25, 'peak_distance': 25},
    'Q':  {'oscilator_limits': np.array([9.000, 12.50]) * 1e9, 'multiplier': 4, 'calibration_nperseg': 40, 'peak_distance': 50},
    'V':  {'oscilator_limits': np.array([12.00, 18.75]) * 1e9, 'multiplier': 4, 'calibration_nperseg': 25, 'peak_distance': 25},
    'W':  {'oscilator_limits': np.array([12.50, 16.70]) * 1e9, 'multiplier': 6, 'calibration_nperseg': 25, 'peak_distance': 25},
}


def get_linearization_reference(shot):
    """Returns a shot and a sweep that can be used to linearize the desired shot.
    
    Paramters
    ---------
    shot: int
        Shot number
    destination_dir: str, optional
        Destination directory for the dump file. Defaults to the current directory.
    
    Returns
    -------
    shot_linearization: int
        Another shot that can be used for linearizing *shot*
    sweep_linearization: int
        The sweep where to look for the initialization.
        
    Raises
    ------
    ValueError
        If there is no linearization configured for the shot. New linearization references should be manually verified and added.
    """
    
    # List interpretation: for shots >= [0], use shot [1] and sweep [2] as reference.
    reference_list = [
        [40608, 40608, 24],  # 12.04.2022 Update calibration tables for pre-linearization of Ka and V bands
        [39791, 40112, 24],  # 01.11.2021 Begining of 2022 campaign
        [36776, 36776, 24], 
        [36468, 36468, 24], 
#         [36370, 36370, 24],  # This shot does not exist. idk what happened
        [35101, 35101, 24],
        [34173, 34173, 24],
        [33816, 33816, 24],  # First shot with today's ramps 
    ]
            
    reference_list.sort(key=lambda x : x[0], reverse=True)
    
    for x in reference_list:
        if shot >= x[0]:
            return x[1], x[2]
        
    raise ValueError(f"No linearization configured for shot {shot}")


@dataclass
class Band:
    name: str
    oscilator_limits: np.ndarray = 0
    probing_limits: np.ndarray = 0
    multiplier: int = 1
    markers: np.ndarray = 0
    expected_markers: np.ndarray = 0
    nperseg: int = 0
    peak_distance: int = 0

    def __init__(self, name):

        self.name = name

        self.oscilator_limits = band_settings[self.name]['oscilator_limits']
        self.multiplier = band_settings[self.name]['multiplier']
        self.peak_distance = band_settings[self.name]['peak_distance']
        self.nperseg = band_settings[self.name]['calibration_nperseg']

        all_markers = np.arange(0.125, 20, 0.25) * 1e9

        self.markers = all_markers[
            (all_markers > self.oscilator_limits[0])
        ]

        self.expected_markers = all_markers[
            (all_markers > self.oscilator_limits[0]) & (
                all_markers < self.oscilator_limits[-1])
        ]

        self.probing_limits = self.oscilator_limits * self.multiplier


def _linearization(markers_signal, markers_values, calibration_signal, sampling_frequency, delay_line, spectrogram_kw, find_peaks_kw, debug):
    """
    Core linearization function. Can be used for debugging purposes.

    Parameters
    ----------
        markers_signal: ndarray
            1D array of the markers signal.

        markers_values: ndarray
            1D array of the frequency values associated with each marker in Hz. 
            The number of elements on this array should be at least as big than the number of peaks in `markers_signal`.
            The last values are discarded if there are more elements than peaks in `markers_signal`.

        calibration_signal: ndarray
            1D array with the calibration signal.

        sampling_frequency: float
            Sampling frequency of the acquisition system.

        delay_line: float
            Time delay in seconds of the delay line used to produce the calibration signal. 

        spectrogram_kw: dictionary
            Dictionary of aditional keyword arguments passed to scipy.signal.spectrogram besides the sampling frequency.

        find_peaks_kw: dictionary
            Dictionary of keyword arguments passed to scipy.signal.find_peaks.

        debug: bool
            If True, returns all intermediate computations.

    Returns
    -------
        frequency: ndarray
            1D array of the absolute frequency in Hz that the system is at for each acquisition sample.
    """

    # Define the time axis of acquisition (used in intermediate calculations)
    time = np.arange(len(markers_signal)) / sampling_frequency

    # Spectrogram of the calibration signal & beat frequency
    y, x, z = spectrogram(calibration_signal,
                          fs=sampling_frequency, **spectrogram_kw)
    
    # Find the beat frequency by maximum search or by fitting a parabola
    # beat_frequency = y[np.argmax(z, axis=0)]
    from .column_wise_max_with_quadratic_interpolation import column_wise_max_with_quadratic_interpolation
    beat_frequency, _ = column_wise_max_with_quadratic_interpolation(z, y)

    # Resample the beat frequency on the time axis, artificially extending the sides (using a constant value)
    beat_frequency = interp1d(x, beat_frequency, kind='cubic', bounds_error=False,
                              fill_value=(beat_frequency[0], beat_frequency[-1]))(time)

    # Calculate the cumulative integral to obtain the relative frequency-time curve
    cum = cumulative_trapezoid(beat_frequency, time) / delay_line
    cum = np.insert(cum, 0, 0.0)  # Manually add a zero to the begining

    # Find the peaks corresponding to the markers
    peaks, properties = find_peaks(markers_signal, **find_peaks_kw)

    # Find the absolute frequency-time curve by least-squares fitting
    # Error is (sampling period) x (frequency rate)
    error = time[1] * np.take(beat_frequency, peaks) / delay_line
    offset = - np.sum((np.take(cum, peaks) -
                      markers_values[:len(peaks)]) / error**2) / np.sum(1/error**2)

    frequency = cum + offset

    if not debug:
        return frequency
    else:
        return frequency, time, x, y, z, beat_frequency, cum, peaks


def get_frequency_time_curve(markers_signal, markers_values, calibration_signal, sampling_frequency, delay_line, spectrogram_kw, find_peaks_kw):
    """
    Get the frequency versus time characteristic of a given band.
    This routine uses scipy.signal.find_peaks to identify the markers and a spectrogram to determine the instantaneous sweep rate of the oscilator.

    Parameters
    ----------
        markers_signal: ndarray
            1D array of the markers signal.

        markers_values: ndarray
            1D array of the frequency values associated with each marker in Hz. 
            The number of elements on this array should be at least as big than the number of peaks in `markers_signal`.
            The last values are discarded if there are more elements than peaks in `markers_signal`.

        calibration_signal: ndarray
            1D array with the calibration signal.

        sampling_frequency: float
            Sampling frequency of the acquisition system.

        delay_line: float
            Time delay in seconds of the delay line used to produce the calibration signal. 

        spectrogram_kw: dictionary
            Dictionary of aditional keyword arguments passed to scipy.signal.spectrogram besides the sampling frequency.

        find_peaks_kw: dictionary
            Dictionary of keyword arguments passed to scipy.signal.find_peaks.

    Returns
    -------
        frequency: ndarray
            1D array of the absolute frequency in Hz that the system is at for each acquisition sample.
    """
    return _linearization(markers_signal, markers_values, calibration_signal, sampling_frequency, delay_line, spectrogram_kw, find_peaks_kw, debug=False)


def get_dynamic_calibration_signals(shot, sweep, band_name=None, shotfile_dir=None):
    """Get the raw signals responsible for the dynamic calibration proccess, i.e., the "Markers" and "Freq. Cal." signals.

    Parameters
    ----------
        shot: int
            Shot number
        sweep: int
            Sweep number
        band_name: one of {'K', 'Ka', 'Q', 'V', 'W'}, optional
            If specified, will overwrite *sweep* with the closest previous sweep corresponding to the band with name *band_name*
        shotfile_dir: str, optional
            Specify the shotfile directory if it is not in the default directory in AFS, i.e., /afs/ipp-garching.mpg.de/home/a/augd/rawfiles/RTR/####/#####/

    Returns
    -------
        markers: ndarray
            "Markers" signal corresponding to the value in *sweep* or the adjusted value according to *band_name*.
        freq_cal: ndarray
            Same as *markers* but for the "Freq. Cal." signal.
    """

    if band_name is None:
        target_sweep = sweep

    else:
        # Get the nearest previous calibration sweep corresponding the the desired band
        positional_index = {'Ka': 0, 'Q': 1, 'V': 2, "W": 3, 'K': 4}
        if sweep > positional_index[band_name]:
            target_sweep = (
                (sweep - positional_index[band_name]) // 5) * 5 + positional_index[band_name]
        else:
            target_sweep = positional_index[band_name]

    # Get the shotfile directory
    if shotfile_dir is None:
        shotfile_dir = get_default_shotfile_dir(shot)

    # Get the markers signal
    markers_file = os.path.join(shotfile_dir, f"RTR_{shot}_Markers.sig")
    # markers_file = os.path.join(shotfile_dir, f"RTR_{shot}_Freq_Cal.sig")
    markers = np.fromfile(file=markers_file, dtype=np.uint16,
                          count=1024, sep="", offset=target_sweep*2048)

    # Get the calibration signal
    calibration_file = os.path.join(shotfile_dir, f"RTR_{shot}_Freq_Cal.sig")
    # calibration_file = os.path.join(shotfile_dir, f"RTR_{shot}_Markers.sig")
    calibration = np.fromfile(
        file=calibration_file, dtype=np.uint16, count=1024, sep="", offset=target_sweep*2048)

    return markers, calibration


def get_linearization(shot, sweep, band_name, debug=False, shotfile_dir=None, spectrogram_window="boxcar", spectrogram_nfft=2048):

  
    
        

    sampling_frequency = get_sampling_frequency(shot, shotfile_dir)

    markers, calibration = get_dynamic_calibration_signals(
        shot, sweep, band_name, shotfile_dir)

    band = Band(band_name)

    spectrogram_kw = {
        "nperseg": band.nperseg, "noverlap": band.nperseg - 1,
        "nfft": spectrogram_nfft, "window": spectrogram_window,
        }

    return band.multiplier * get_frequency_time_curve(
        markers, band.markers, calibration, sampling_frequency, 14.08e-9,
        spectrogram_kw,
        {"prominence": 100, "distance": band.peak_distance},
    )


def linearize(x, y):
    """
    Interpolates data using a cubic spline and returns a linearized version of the data.

    Parameters:
    ----------
    x : array-like, shape (n,)
        The independent variable values (must be sorted in ascending order).
    y : array-like, shape (n,) or (m, n)
        The dependent variable values, either a 1D array of shape (n,) or 
        a 2D array of shape (m, n) where each column is a separate dataset.

    Returns:
    -------
    new_x : ndarray, shape (n,)
        The new independent variable values, linearly spaced.
    new_y : ndarray, shape (n,) or (m, n)
        The interpolated dependent variable values corresponding to `new_x`.
    """
    # Create new x values linearly spaced
    new_x = np.linspace(x[0], x[-1], len(x))
    
    # Interpolate using CubicSpline
    cs = CubicSpline(x, y, axis=-1)  # Use axis=0 to handle both 1D and 2D cases
    new_y = cs(new_x)

    return new_x, new_y
