import numpy as np
from numpy.lib.stride_tricks import sliding_window_view


def burst_mode_handler(times, start, end, time_step, burst):
    """Calculate the sweeps that are needed to process and create a convolution kernel to average the spectrograms in burst mode
    
    Parameters
    ----------
    times: ndarray
        Array of time instants taken from the acquisition. This array must contain evenly-spaced samples.
    start: float
        Starting time for the burst mode analysis
    end: float
        End time for the burst mode analysis
    time_step: float
        Desired time step for the burst mode analysis
    burst: int
        Number of bursts to use. Must be odd.
        
    Returns
    -------
    all_sweeps: ndarray
        Array of all the sweeps that are necessary to process
    central_sweeps: ndarray
        Array of the sweeps that are in the center of each burst window
    burst_mode: function
        Function to convolute the signals according to the input parameters.
        Example usage:
        >>> burst_spectrograms = burst_mode(all_spectrograms)
    """


    acquisition = times[1] - times[0]
    first_time = times[0]
    last_time = times[-1]


    # Round *time step* to a multiple of *acquisition*
    time_step = acquisition * round(time_step / acquisition)
    if time_step == 0: 
        time_step = acquisition
    sweep_step = round(time_step / acquisition)

#     print(f"Actual time step converted to {time_step}, sweep step is {sweep_step}")

    # Check if the combination of starting time, ending time and burst complies with the avaliable sweeps
    if (start - burst // 2 * acquisition) < first_time:
#         print(f"WARNING: Requested first sweep at {start - burst // 2 * acquisition} does not exist, starting time adjusted to {first_time + burst // 2 * acquisition}")
        start = first_time + burst // 2 * acquisition   
    if (end + burst // 2 * acquisition) > last_time:
#         print(f"WARNING: Requested last sweep at {end + burst // 2 * acquisition} does not exist, ending time adjusted to {last_time - burst // 2 * acquisition}")
        end = last_time - burst // 2 * acquisition

    if (end - start) < acquisition:
#         print(f"WARNING: (end_time - start_time) is smaller than the acquisition time step. Adjusted end time to {start + acquisition}")
        end = start + acquisition

    central_sweeps = np.arange(round(start / acquisition), round(end / acquisition) + 1, sweep_step)

    # Check the implication of burst mode (no overlap, contiguos, overlaped, extremely overlaped)
    if burst < sweep_step:  # No overlap
        all_sweeps = [np.arange(sweep - burst // 2, sweep + burst // 2 + 1) for sweep in central_sweeps]
        all_sweeps = np.array(all_sweeps).flatten()
        
        # Create the function that will sum the spectrograms according to the selected temporal sampling and burst size
        burst_mode = lambda x: np.average(sliding_window_view(x, window_shape=burst, axis=0)[::burst], axis=-1)

    else: # Contiguos, overlaped, or extremely overlaped
        all_sweeps = np.arange(central_sweeps[0] - burst // 2, central_sweeps[-1] + burst // 2 + 1)

        # Create the function that will sum the spectrograms according to the selected temporal sampling and burst size
        burst_mode = lambda x: np.average(sliding_window_view(x, window_shape=burst, axis=0)[::sweep_step], axis=-1)

    return all_sweeps, central_sweeps, burst_mode