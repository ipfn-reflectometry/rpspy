import numpy as np
import getpass
from .profile_inversion import ne_to_f

def write_shotfile(time, density, lfs_area, hfs_area, lfs_gd, hfs_gd, filters, spectrogram_parameters, group_delay_calibration, exclusion_regions, shot, type='private'):
    """Write the reflectometry data to a shotfile under experiment REFG.

    Parameters
    ----------
    time : numpy.ndarray (T,)
        Time points of the reflectometry data in units of s.
    density : numpy.ndarray (D,)
        Plasma density axis sampled on 151 points in units of m^-3.
    lfs_area : numpy.ndarray (T, D,)
        2D array of position as function of time and frequency/density for the LFS reflectometer.
    hfs_area : numpy.ndarray (T, D,)
        2D array of position as function of time and frequency/density for the HFS reflectometer.
    lfs_gd : numpy.ndarray (T, D,)
        Group delay as function of time and frequency/density for the LFS reflectometer.
    hfs_gd : numpy.ndarray (T, D,)
        Group delay as function of time and frequency/density for the HFS reflectometer.
    filters : dict
        Dictionary containing the filter parameters for the shotfile according to the following template.
        Filters values should counted from the dispersion line and normalized to the Nyquist frequency, i.e., [0, 1].
            filters = {
                "HFS": {
                    "K": [
                        (HFS_K_lower_filter - dispersion) / Nyquist_frequency,
                        (HFS_K_upper_filter - dispersion) / Nyquist_frequency
                    ],
                    "Ka": [
                        (HFS_Ka_lower_filter - dispersion) / Nyquist_frequency,
                        (HFS_Ka_upper_filter - dispersion) / Nyquist_frequency
                    ],
                    "Q": [
                        (HFS_Q_lower_filter - dispersion) / Nyquist_frequency,
                        (HFS_Q_upper_filter - dispersion) / Nyquist_frequency
                    ],
                    "V": [
                        (HFS_V_lower_filter - dispersion) / Nyquist_frequency,
                        (HFS_V_upper_filter - dispersion) / Nyquist_frequency
                    ]
                },
                "LFS": {
                    "K": [
                        (LFS_K_lower_filter - dispersion) / Nyquist_frequency,
                        (LFS_K_upper_filter - dispersion) / Nyquist_frequency
                    ],
                    "Ka": [
                        (LFS_Ka_lower_filter - dispersion) / Nyquist_frequency,
                        (LFS_Ka_upper_filter - dispersion) / Nyquist_frequency
                    ],
                    "Q": [
                        (LFS_Q_lower_filter - dispersion) / Nyquist_frequency,
                        (LFS_Q_upper_filter - dispersion) / Nyquist_frequency
                    ],
                    "V": [
                        (LFS_V_lower_filter - dispersion) / Nyquist_frequency,
                        (LFS_V_upper_filter - dispersion) / Nyquist_frequency
                    ]
                }
            }
        spectrogram_parameters : dict
            Dictionary containing the parameters for the spectrogram calculation according to the following template.
            spectrogram_parameters = {
                "HFS": {
                    "K": {
                        "nperseg": HFS K-band DFT window size,
                        "noverlap": HFS K-band DFT window overlap,
                    },
                    "Ka": {
                        "nperseg": HFS Ka-band DFT window size,
                        "noverlap": HFS Ka-band DFT window overlap,
                    },
                    "Q": {
                        "nperseg": HFS Q-band DFT window size,
                        "noverlap": HFS Q-band DFT window overlap,
                    },
                    "V": {
                        "nperseg": HFS V-band DFT window size,
                        "noverlap": HFS V-band DFT window overlap,
                    }
                },
                "LFS": {
                    "K": {
                        "nperseg": LFS K-band DFT window size,
                        "noverlap": LFS K-band DFT window overlap,
                    },
                    "Ka": {
                        "nperseg": LFS Ka-band DFT window size,
                        "noverlap": LFS Ka-band DFT window overlap,
                    },
                    "Q": {
                        "nperseg": LFS Q-band DFT window size,
                        "noverlap": LFS Q-band DFT window overlap,
                    },
                    "V": {
                        "nperseg": LFS V-band DFT window size,
                        "noverlap": LFS V-band DFT window overlap,
                    }
                }
            }
        group_delay_calibration : dict
            Dictionary containing the group delay calibration parameters according to the following template.
            Calibration equation is: group_delay_zero = A + B * sqrt(1-(f/fc) ** 2),
            where f is the frequency [Hz], fc is the cutoff frequency [Hz], and A and B are calibration constants [s].
            
            group_delay_calibration = {
                "HFS": {
                    "K": {
                        "A": HFS K-band calibration 'A' constant,
                        "B": HFS K-band calibration 'B' constant,
                        "fc": HFS K-band calibration 'fc' constant,
                    },
                    "Ka": {
                        "nperseg": HFS Ka DFT window size,
                        "noverlap": HFS Ka DFT window overlap,
                    },
                    "Q": {
                        "nperseg": HFS Q DFT window size,
                        "noverlap": HFS Q DFT window overlap,
                    },
                    "V": {
                        "nperseg": HFS V DFT window size,
                        "noverlap": HFS V DFT window overlap,
                    }
                },
                "LFS": {
                    "K": {
                        "nperseg": LFS K DFT window size,
                        "noverlap": LFS K DFT window overlap,
                    },
                    "Ka": {
                        "nperseg": LFS Ka DFT window size,
                        "noverlap": LFS Ka DFT window overlap,
                    },
                    "Q": {
                        "nperseg": LFS Q DFT window size,
                        "noverlap": LFS Q DFT window overlap,
                    },
                    "V": {
                        "nperseg": LFS V DFT window size,
                        "noverlap": LFS V DFT window overlap,
                    }
                }
            }
        exclusion_regions : dict
            Dictionary containing the exclusion regions for the shotfile according to the following template.
            exclusion_regions = {
                "HFS": [
                    [exclusion_region_1_start, exclusion_region_1_end],
                    [exclusion_region_2_start, exclusion_region_2_end],
                    ...
                ]
                "LFS": [
                    [exclusion_region_1_start, exclusion_region_1_end],
                    [exclusion_region_2_start, exclusion_region_2_end],
                    ...
                ]
            }
        shot : int
            Shot number of the reflectometry data.        
        type : str
            Type of the shotfile. Options are "private" or "public". Default is 'private'.
            If private, will write the shotfile under the current user private shotfile directory.
            If public, will write the shotfile under the AUGD experiment directory.
    
    """

    import aug_sfutils as sf

    # Number of time points
    time_points = len(time)
    print(f"Number of time points: {time_points}")

    # Number of frequency points
    frequency_points = lfs_area.shape[1]
    print(f"Number of frequency points: {frequency_points}")

    shotfile_parameter_sets = {'Filters': {}, 'sFFTcfg': {}, 'Calibs': {}}

    # Parse filters dictionary
    # Append zeros at the end for out-of-service W-band
    shotfile_parameter_sets['Filters'] = {
    'HFS_lc': [filters['HFS'][key][0] for key in ['K', 'Ka', 'Q', 'V']] + [0],
	'HFS_hc': [filters['HFS'][key][1] for key in ['K', 'Ka', 'Q', 'V']] + [0],
	'LFS_lc': [filters['LFS'][key][0] for key in ['K', 'Ka', 'Q', 'V']] + [0],
	'LFS_hc': [filters['LFS'][key][1] for key in ['K', 'Ka', 'Q', 'V']] + [0],

    # No clue what these are, let it be lists of 5 zeros
	'HFS_N': 5 * [0],
	'HFS_A': 5 * [0],
	'LFS_N': 5 * [0],
	'LFS_A': 5 * [0],
    }

    # Parse spectrogram parameters dictionary
    # Append zeros at the end for out-of-service W-band
    shotfile_parameter_sets['sFFTcfg'] = {
        'HFSWsize': [int(spectrogram_parameters['HFS'][key]['nperseg']) for key in ['K', 'Ka', 'Q', 'V']] + [0],
        'HFSSstep': [int(spectrogram_parameters['HFS'][key]['nperseg'] - spectrogram_parameters['HFS'][key]['noverlap']) for key in ['K', 'Ka', 'Q', 'V']] + [0],
        'LFSWsize': [int(spectrogram_parameters['LFS'][key]['nperseg']) for key in ['K', 'Ka', 'Q', 'V']] + [0],
        'LFSSstep': [int(spectrogram_parameters['LFS'][key]['nperseg'] - spectrogram_parameters['LFS'][key]['noverlap']) for key in ['K', 'Ka', 'Q', 'V']] + [0],
    }

    # Parse group delay calibration dictionary
    # Append zeros at the end for out-of-service W-band
    shotfile_parameter_sets['Calibs'] = {
        'HFSecorr': 5 * [0.],  # Ad-hoc correction for HFS K, Ka, Q, V, and W bands
        'LFSecorr': 5 * [0.],  # Ad-hoc correction for LFS K, Ka, Q, V, and W bands	
        'HFScor_V': 5 * [0.],  # "Vaccumm" correction for HFS K, Ka, Q, V, and W bands
        'LFScor_V': 5 * [0.],  # "Vaccumm" correction for LFS K, Ka, Q, V, and W bands

        # Value of constant 'A' in the dispersion equation for HFS K, Ka, Q, V, and W bands
        'HFScor_A': [group_delay_calibration['HFS'][band]['A'] for band in ['K', 'Ka', 'Q', 'V']] + [0],  
        # Value of constant 'A' in the dispersion equation for LFS K, Ka, Q, V, and W bands
        'LFScor_A': [group_delay_calibration['LFS'][band]['A'] for band in ['K', 'Ka', 'Q', 'V']] + [0],

        # Value of constant 'B' in the dispersion equation for HFS K, Ka, Q, V, and W bands
        'HFScor_B': [group_delay_calibration['HFS'][band]['B'] for band in ['K', 'Ka', 'Q', 'V']] + [0],
        # Value of constant 'B' in the dispersion equation for LFS K, Ka, Q, V, and W bands
        'LFScor_B': [group_delay_calibration['LFS'][band]['B'] for band in ['K', 'Ka', 'Q', 'V']] + [0],

        # Value of constant 'fc' in the dispersion equation for HFS K, Ka, Q, V, and W bands
        'HFScorfc': [group_delay_calibration['HFS'][band]['fc'] for band in ['K', 'Ka', 'Q', 'V']] + [0],
        # Value of constant 'fc' in the dispersion equation for LFS K, Ka, Q, V, and W bands
        'LFScorfc': [group_delay_calibration['LFS'][band]['fc'] for band in ['K', 'Ka', 'Q', 'V']] + [0],

        # Begining of the 10 frequency exclusion regions on the HFS
        ''

        'HFSfix_i': 10 * [0.], # Begining of the 10 frequency exclusion regions on the HFS
        'HFSfix_f': 10 * [0.], # End of the 10 frequency exclusion regions on the HFS
        'LFSfix_i': 10 * [0.], # Begining of the 10 frequency exclusion regions on the LFS
        'LFSfix_f': 10 * [0.], # End of the 10 frequency exclusion regions on the LFS
    }

    for side in ['HFS', 'LFS']:
        for i, x in enumerate(exclusion_regions[side]):
            print(x)
            shotfile_parameter_sets['Calibs'][f'{side}fix_i'][i] = float(x[0]) * 1e-9
            shotfile_parameter_sets['Calibs'][f'{side}fix_f'][i] = float(x[1]) * 1e-9



    rps_shotfile_dictionary = {
        # Time array
        'TIME': time,

        # 2D array of position as function of time and density
        'RB_HFS': hfs_area,
        'RB_LFS': lfs_area,

        # Group delay as function of time and frequency
        'GD_HFS': hfs_gd,
        'GD_LFS': lfs_gd,

        # Density axis broadcast to matrix. 
        # It's a waste of memory, but it's the only way to make this compatible with our shotfile header.
        'neb_HFS': np.broadcast_to(density, hfs_area.shape),
        'neb_LFS': np.broadcast_to(density, hfs_area.shape),

        # Position of zero density. 
        # It's a waste of memory, but it's the only way to make this compatible with our shotfile header.
        'X0_HFS': hfs_area[:,0],
        'X0_LFS': lfs_area[:,0],

        # Frequency axis broadcast to matrix.
        # It's a waste of memory, but it's the only way to make this compatible with our shotfile header.
        'FP_HFS': np.broadcast_to(ne_to_f(density), hfs_area.shape),
        'FP_LFS': np.broadcast_to(ne_to_f(density), hfs_area.shape),

        'Filters': shotfile_parameter_sets['Filters'],

        'sFFTcfg': shotfile_parameter_sets['sFFTcfg'],	

        'Calibs': shotfile_parameter_sets['Calibs'],
    }

    sf.write_sf(
        shot, rps_shotfile_dictionary, '/shares/departments/AUG/users/danielhfc/', 'RPS', 
        exp=getpass.getuser().capitalize() if type == 'private' else 'AUGD',
        )

