import os
import numpy as np
from scipy.interpolate import griddata, interp1d
from rpspy import profile_inversion, f_to_ne, get_antenna_position, get_sampling_frequency, get_linearization_reference, get_default_shotfile_dir, Band, batch_processing
import scipy.constants as cst
from ipfnpytools.rps_dump import write_dump
import typer
    

def full_profile_reconstruction(shot: int, destination_dir: str='.', start_time: float=0.0, end_time: float=10.0, time_step: float=1e-3, burst: int=29):
    """Semi automated script to perform the full profile reconstruction. Results are written to a dump file with Jorge's architecture.
    
    Parameters
    ----------
    shot: int
        Shot number
    """
    
    
    # General processing parameters
#     shotfile_dir = f"/afs/ipp-garching.mpg.de/home/a/augd/rawfiles/RTR/{shot//10}/{shot}/"
    # shotfile_dir = f"/afs/ipp-garching.mpg.de/home/d/danielhfc/shotfiles/{shot}/"
    shotfile_dir = get_default_shotfile_dir(shot)
    
    if not os.path.exists(shotfile_dir):
        raise ValueError(f"Raw data for shot {shot} was not found.") 
    

    sampling_frequency = get_sampling_frequency(shot, shotfile_dir)

    shot_linearization, sweep_linearization = get_linearization_reference(shot)
#     linearization_shotfile_dir = f"/afs/ipp-garching.mpg.de/home/a/augd/rawfiles/RTR/{shot_linearization//10}/{shot_linearization}/"
    linearization_shotfile_dir = get_default_shotfile_dir(shot_linearization)


    # Define inner and outer limiter positions for initialization -------------------------------------
    inner_limiter = 1.052
    outer_limiter = 2.206

    hfs_gd_zero = 2 * (inner_limiter - get_antenna_position('hfs')) / cst.c
    lfs_gd_zero = 2 * (get_antenna_position('lfs') - outer_limiter) / cst.c

    # Use these bands
    used_bands = [
        ('K', 'HFS', 'real'), 
        ('Ka', 'HFS', 'real'), 
        ('Q', 'HFS', 'real'), 
    #     ('V', 'HFS', 'complex'),
        ('K', 'LFS', 'real'), 
        ('Ka', 'LFS', 'real'), 
        ('Q', 'LFS', 'real'), 
    #     ('V', 'LFS', 'complex')
    ]
    
    # Spectrogram options
    spectrogram_options = {
        'K': {'nperseg': 128, 'noverlap':96, 'nfft': 1024},
        'Ka': {'nperseg': 128, 'noverlap':96, 'nfft': 1024},
        'Q': {'nperseg': 128, 'noverlap':96, 'nfft': 1024},
        'V': {'nperseg': 32, 'noverlap':24, 'nfft': 1024},
    }

    # Apply filters a la Jorge ----------------------
    filters = {
        'HFS': {
            'K': [0.0e6, 4.0e6],
            'Ka': [0.0e6, 4.0e6],
            'Q': [0.0e6, 4.0e6],
            'V': [0.0e6, 6.0e6],
        },
        'LFS': {
            'K': [0.2e6, 4.0e6],
            'Ka': [0.0e6, 4.0e6],
            'Q': [0.0e6, 4.0e6],
            'V': [0.8e6, 6.0e6],
        },
    }

    subtract_on_bands = ['Q-HFS', 'Q-LFS']

    # Read the raw data -------------------------------------------
    bands = [Band(name, side, dtype) for name, side, dtype in used_bands]

    for band in bands:
        band.upper_filter = filters[band.side][band.name][1]
        band.lower_filter = filters[band.side][band.name][0]
        band.nperseg = spectrogram_options[band.name]['nperseg']
        band.noverlap = spectrogram_options[band.name]['noverlap']
        band.nfft = spectrogram_options[band.name]['nfft']

    for band in bands:

        times, band.freq, band.gd = batch_processing(shot, band.name, band.side, band.dtype, start_time, end_time, time_step, burst, band.lower_filter, band.upper_filter, subtract_on_bands, shotfile_dir, shot_linearization, linearization_shotfile_dir, sweep_linearization, nperseg=band.nperseg, nfft=band.nfft, noverlap=band.noverlap)

    # -----------------------------------
    # MERGE TOGETHER ALL BANDS
    # -----------------------------------

    hfs_first_freq = np.inf
    lfs_first_freq = np.inf

    hfs_last_freq = -np.inf
    lfs_last_freq = -np.inf

    for band in bands:
        if band.side == 'HFS':
            hfs_first_freq = min(band.freq[0], hfs_first_freq)
            hfs_last_freq = max(band.freq[-1], hfs_last_freq)
        elif band.side == 'LFS':
            lfs_first_freq = min(band.freq[0], lfs_first_freq)
            lfs_last_freq = max(band.freq[-1], lfs_last_freq)

    hfs_all_freq = np.concatenate([band.freq for band in bands if band.side == 'HFS'])
    lfs_all_freq = np.concatenate([band.freq for band in bands if band.side == 'LFS'])

    hfs_all_gd = np.concatenate([band.gd for band in bands if band.side == 'HFS'], axis=1)
    lfs_all_gd = np.concatenate([band.gd for band in bands if band.side == 'LFS'], axis=1)

    # Filter out certain frequencies -----------------------------------------------------------------

    # Set up exclusion regions
    exclude_hfs =[
#         [36.25e9, 38.5e9],
    #     [47.5e9, 61e9],
    #     [70.0e9, 100e9],
    ]

    exclude_lfs =[
    #     [47e9, 58e9],
    ]

    print(times.shape)
    print(hfs_all_freq.shape)
    print(hfs_all_gd.shape)

    for l, u in exclude_hfs:
        hfs_all_gd = hfs_all_gd[:, (hfs_all_freq < l) | (hfs_all_freq > u)]
        hfs_all_freq = hfs_all_freq[(hfs_all_freq < l) | (hfs_all_freq > u)]

    for l, u in exclude_lfs:
        lfs_all_gd = lfs_all_gd[:, (lfs_all_freq < l) | (lfs_all_freq > u)]
        lfs_all_freq = lfs_all_freq[(lfs_all_freq < l) | (lfs_all_freq > u)]

    hfs_resampled_freq = np.linspace(hfs_first_freq*1.0001, hfs_last_freq*0.9999, 1000)
    lfs_resampled_freq = np.linspace(lfs_first_freq*1.0001, lfs_last_freq*0.9999, 1000)

    hfs_resampled_gd = interp1d(
        x=hfs_all_freq,
        y=hfs_all_gd,
        kind='linear',
        fill_value=np.nan,
        axis=-1,
    )(hfs_resampled_freq)

    print(times.shape)
    print(hfs_resampled_freq.shape)
    print(hfs_resampled_gd.shape)

    lfs_resampled_gd = interp1d(
        x=lfs_all_freq,
        y=lfs_all_gd,
        kind='linear',
        fill_value=np.nan,
        axis=-1,
    )(lfs_resampled_freq)

    # ----------------------------------------------------------
    # INITIALIZE AT THE LIMITER
    # ----------------------------------------------------------

    def linear_initialization(fp, gd, gd_at_zero, up_to):

        n_points = 20  # Number of points in the initialization

        idx = fp < up_to  # Only use first points for linear initialization

        xi = np.sum(fp[idx], axis=-1)
        xi_xi = np.sum(fp[idx] * fp[idx], axis=-1)
        xi_yi = np.sum(gd[:, idx] * fp[idx], axis=-1)

        slope = (xi_yi - gd_at_zero * xi) / xi_xi


        fp_extrapolated = np.append(np.linspace(0, fp[0], n_points)[:-1], fp)
        gd_extrapolated = np.append(slope[:, None] * fp_extrapolated[:n_points - 1] + gd_at_zero, gd, axis=-1)

        return fp_extrapolated, gd_extrapolated


    fp_hfs, gd_hfs = linear_initialization(hfs_resampled_freq, hfs_resampled_gd, hfs_gd_zero, up_to=19.5e9)

    fp_lfs, gd_lfs = linear_initialization(lfs_resampled_freq, lfs_resampled_gd, lfs_gd_zero, up_to=19.5e9)

    # -------------------------------------------------------------
    # PROFILE INVERSION
    # -------------------------------------------------------------

    hfs_antenna = get_antenna_position('hfs')
    lfs_antenna = get_antenna_position('lfs')

    # Resample frequency and group delay to 0.5 GHz spacing
    # f_hfs = np.arange(0, fp_hfs.max(), 0.5e9)
    f_hfs = np.linspace(0, 75e9, 151)
    gds_hfs = np.array([griddata(fp_hfs, gd, f_hfs) for gd in gd_hfs])

    # f_lfs = np.arange(0, fp_lfs.max(), 0.5e9)
    f_lfs = np.linspace(0, 75e9, 151)
    gds_lfs = np.array([griddata(fp_lfs, gd, f_lfs) for gd in gd_lfs])

    # Compute the abel inverse
    # r_hfs = np.array([profile_inversion(f_hfs, gd) for gd in gds_hfs])
    # r_hfs = r_hfs + hfs_antenna

    # r_lfs = np.array([profile_inversion(f_lfs, gd) for gd in gds_lfs])
    # r_lfs =  - r_lfs + lfs_antenna

    # With Dirk's help :)
    r_hfs = profile_inversion(f_hfs, np.clip(gds_hfs.T - hfs_gd_zero, a_min=0, a_max=np.inf), pwld_batch=True).T
    r_hfs = r_hfs + inner_limiter

    r_lfs = profile_inversion(f_lfs, np.clip(gds_lfs.T - lfs_gd_zero, a_min=0, a_max=np.inf), pwld_batch=True).T
    r_lfs =  - r_lfs + outer_limiter

    # ------------------------------------------------------------
    # Write dump file
    # ------------------------------------------------------------

    def t_to_s(time):
        """ Time to string, e.g., 3.5 -> '3s5' """
        return f"{time:.1f}".replace('.', 's')
    
    if not os.path.exists(destination_dir):
        try:
            os.makedirs(destination_dir)
        except OSError as e:
            # Handle any errors that may occur during directory creation
            raise Exception(f"Error creating directory '{destination_dir}': {e}")

    write_dump(times, f_hfs, f_to_ne(f_hfs), r_hfs, r_lfs, gds_hfs, gds_lfs, f"{destination_dir}/{t_to_s(times[0])}_{t_to_s(times[-1])}.{shot}")
    
    
if __name__ == '__main__':
    typer.run(full_profile_reconstruction)
#     full_profile_reconstruction(int(sys.argv[1]), float(sys.argv[2]), float(sys.argv[3]))