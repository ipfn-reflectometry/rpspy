##################################################
# Look for inconcistencies in the rawfiles
##################################################
import xml.etree.ElementTree as ET
import numpy as np
from .read_rtr_data import get_default_shotfile_dir


def check_time_file(shot):
    
    shotfile_dir = get_default_shotfile_dir(shot)
    
    tree = ET.parse(f"{shotfile_dir}RTR_{shot}_AcquisitionConfiguration.xml")
    sweep_period = float(tree.find("AcquisitionTimmingConfiguration").find("sweep_period").text)
    
    # Read time file
    file_name = f"{shotfile_dir}RTR_{shot}_Sweep_double.tb"
    time = np.fromfile(file=file_name, dtype=np.float64, sep="")
    
    return np.isclose(time, np.arange(len(time))*sweep_period*1e-6 + time[0]).all()