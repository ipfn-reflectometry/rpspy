import numpy as np
import matplotlib.pyplot as plt

def get_reflectometry_limiter_position(side):

    # Define inner and outer limiter positions for initialization -------------------------------------
    inner_limiter = 1.052
    outer_limiter = 2.206

    if side.lower() == 'hfs':
        return inner_limiter
    
    elif side.lower() == 'lfs':
        return outer_limiter
    
    else:
        raise ValueError("argument `side` must be either 'LFS' for low-field side or 'HFS' for high-field side")

def get_antenna_position(side, antenna="frontmost"):
    """Get the antenna position
    
    Parameters
    ----------
    side: str {'HFS', 'LFS'}
        High-field side (HFS) or low-field side (LFS)
    antenna: str {'frontmost', 'K', 'Ka', 'Q', 'V', 'W'}
        Antenna name. If 'frontmost', will return the antenna closer to the plasma.  

    Returns
    -------
    position: float
        Antenna position in meters in tokamak coordinate major radius.
    """

    # Based on A. Silva Ph.D. thesis
    # lfs = {'K': 2.375, 'Ka': 2.328, 'Q': 2.365, 'V': 2.374, 'W': 2.341}
    # hfs = {'K': 1.001, 'Ka': 0.989, 'Q': 0.991, 'V': 0.987}

    # Based on CAD drawing 2015
    lfs = {'K': 2.359, 'Ka': 2.335, 'Q': 2.362, 'V': 2.381, 'W': 2.341}
    hfs = {'K': 0.99857, 'Ka': 0.988315, 'Q': 0.99012, 'V': 0.98862}
    hfs = {'K': 0.99857, 'Ka': 0.988315, 'Q': 0.99012, 'V': 0.98862-0.0035}



    if antenna == "frontmost":
        if side.lower() == 'hfs':
            return max(hfs.values())
        elif side.lower() == 'lfs':
            return min(lfs.values())

    else:
        if side.lower() == 'hfs':
            return hfs[antenna]
        elif side.lower() == 'lfs':
            return lfs[antenna]
            

def get_band_cutoff_frequency(band):
    """Get the waveguide cutoff frequency for a given band
    
    Parameters
    ----------
    band: str {'K', 'Ka', 'Q', 'V', 'W'}
        Desired microwave band

    Returns
    -------
    fc: float
        Waveguide cutoff frequency in GHz
    """
    fc = {'K':14.047, 'Ka':21.081, 'Q':26.342, 'V':39.863, 'W':59.010}  # Waveguide cutoff frequency for each band (K, Ka, Q, V, W)
    return fc[band]


def aug_tgcorr2(band, side, f, shotn, novcorr=False, vcorr=0, ws='256', origin='Daniel', verbose=False):
    """Calculate the waveguide dispersion
    
    Parameters
    ----------
    band: str {'K', 'Ka', 'Q', 'V', 'W'}
        Desired microwave band
    side: str {'HFS', 'LFS'}
        High-field side (HFS) or low-field side (LFS)
    f: ndarray (N,)
        Frequencies at which the waveguide dispersion is calculated (in GHz units)
    shotn: int
        Shot number
    novcorr: bool
        Do not apply vacuum correction. If False, will apply the vacuum correction of the current antenna setup.
    vcorr: float
        Additional vacuum correction added to the default vacuum correction.
    ws: str {'32', '64', '128', '256', '512'}
        Window size used on the FFT to calculate the dispersion. Optional, defaults to 256 points.
    
    Returns
    -------
    dispersion: ndarray (N,)
        Waveguide dispersion computed at the frequencies in `f`.
    """
    c = 299792458.0  # light speed in m.s^-1
    
    band_index = {'K':0, 'Ka':1, 'Q':2, 'V':3, 'W':4}[band]
    ws_index = {'32':0, '64':1, '128':2, '256':3, '512':4}[ws]
    
    fc = {}
    for key in ['K', 'Ka', 'Q', 'V', 'W']:
        fc[key] = get_band_cutoff_frequency(key)
    
    if side == 'HFS':
        
        # Based on 2015 CAD scans for K antenna at 0.9986
        # antenna_offset = {'K':0.0, 'Ka':0.0103, 'Q':0.0081, 'V':0.0099, 'W':0.0099}  # Antenna offsets on the HFS for each band (K, Ka, Q, V, W)
        
        # Old position estimates for K antenna at 1.001
        # antenna_offset = {'K':0.0, 'Ka':1.2e-2, 'Q':1.0e-2, 'V':1.4e-2, 'W':0.0}  # Antenna offsets on the HFS for each band (K, Ka, Q, V, W)

        # Dynamically get the antenna offset based on the antenna position
        antenna_offset = {'K':0.0, 'Ka':0.0, 'Q':0.0, 'V':0.0}  # Antenna offsets on the HFS for each band (K, Ka, Q, V, W)
        for key in antenna_offset.keys():
            antenna_offset[key] = get_antenna_position('HFS', antenna='frontmost') - get_antenna_position('HFS', antenna=key)
        
        if shotn >= 41846: # After 2024 restart with new upper divertor (only V band differs from previous)
            pass

        elif shotn >= 39796:  # After October 2021 intervention
            
            # Daniel -------------------------------------------------------------------------------
            if origin == 'Daniel':
                pass
            
            # Jorge -------------------------------------------------------------------------------
            elif origin == 'Jorge':
                pass
            
        else:        
            pass

    
    elif side == 'LFS':
        
        
        # Based on 2015 CAD scans for Ka antenna at 2.3354
        # antenna_offset = {'K':0.0237, 'Ka':0.0000, 'Q':0.0264, 'V':0.0450, 'W':0.0279}  # Antenna offsets on the HFS for each band (K, Ka, Q, V, W)
        
        # Based on 2020 point-cloud scan. For Ka antenna at 2.337 
#         antenna_offset = {'K':1.8e-2, 'Ka':0.0, 'Q':2.3e-2, 'V':4.0e-2, 'W':2.4e-2}  

        # Old position estimates for Ka antenna at 2.328
        # antenna_offset = {'K':4.7e-2, 'Ka':0.0, 'Q':3.7e-2, 'V':4.6e-2, 'W':1.3e-2}  # Antenna offsets on the LFS for each band (K, Ka, Q, V, W)

        # Dynamically get the antenna offset based on the antenna position
        antenna_offset = {'K':0.0, 'Ka':0.0, 'Q':0.0, 'V':0.0}  # Antenna offsets on the HFS for each band (K, Ka, Q, V, W)
        for key in antenna_offset.keys():
            antenna_offset[key] = get_antenna_position('LFS', antenna=key) - get_antenna_position('LFS', antenna='frontmost') 
        
        if shotn >= 41846: # After 2024 restart with new upper divertor (only V band differs from previous)
            pass

        
        elif shotn >= 39796:  # After October 2021 intervention
            
            # Daniel -----------------------------------------------------------------------------------
            if origin == 'Daniel':
            
                fc['V'] = 38

            
            # Jorge ------------------------------------------------------------------------------------
            if origin == 'Jorge':
                pass
        
        
        # Shots before and after 36800 require different treatment on the LFS
        elif shotn >= 36800: 
            pass

        
        else:
            pass

            
    else:
        raise ValueError("argument `side` must be either 'LFS' for low-field side or 'HFS' for high-field side")
        
    tgv = 0 if novcorr else 2 * antenna_offset[band] / c
    tgv += vcorr
    
    # A = A[ws_index][band_index]
    # B = B[ws_index][band_index]

    A, B = get_A_B(shotn, band, side, ws=ws, origin=origin, verbose=verbose)
    
    fc = get_band_cutoff_frequency(band)
    
    if verbose:
        print(f"A: {A}\nB: {B}")
    
    return tgv + (A + B / np.sqrt(1 - (fc / f) ** 2.0))


# def _get_fc(shotn, band, side, origin='Daniel', verbose=False):

#     fc = {'K':14.047, 'Ka':21.081, 'Q':26.342, 'V':39.863, 'W':59.010}  # Waveguide cutoff frequency for each band (K, Ka, Q, V, W)

#     if side == 'LFS':
#          if (shotn >= 39796) and (shotn < 41846):  # After October 2021 intervention before 2024 restart with new upper divertor
#             if origin == 'Daniel':
#                 fc['V'] = 38

#     return fc[band]



def get_A_B(shotn, band, side, ws='256', origin='Daniel', verbose=False):
    """Get the A and B coefficients for the waveguide dispersion calculation

    Parameters
    ----------
    shotn: int
        Shot number
    band: str {'K', 'Ka', 'Q', 'V', 'W'}
        Desired microwave band
    side: str {'HFS', 'LFS'}
        High-field side (HFS) or low-field side (LFS)

    Returns
    -------
    A, B: 2-tuple of float

    Notes
    -----
    Do not change keyword arguments unless you know what you are doing.
    """

    band_index = {'K':0, 'Ka':1, 'Q':2, 'V':3, 'W':4}[band]
    ws_index = {'32':0, '64':1, '128':2, '256':3, '512':4}[ws]

    
    if side == 'HFS':

        if shotn >= 41846: # After 2024 restart with new upper divertor (only V band differs from previous)
            
            A = [
                    [5.95501e-10, 8.99833e-10, 9.19432e-10, -7.001e-8, 0.0], 
                    [5.95501e-10, 8.99833e-10, 9.19432e-10, -7.001e-8, 0.0], 
                    [5.95501e-10, 8.99833e-10, 9.19432e-10, -7.001e-8, 0.0], 
                    [5.95501e-10, 8.99833e-10, 9.19432e-10, -7.001e-8, 0.0], 
                    [5.95501e-10, 8.99833e-10, 9.19432e-10, -7.001e-8, 0.0]
                ]
            B = [
                    [2.28098e-09, 1.37508e-09, 1.42559e-09, 4.943e-8, 0.0], 
                    [2.28098e-09, 1.37508e-09, 1.42559e-09, 4.943e-8, 0.0], 
                    [2.28098e-09, 1.37508e-09, 1.42559e-09, 4.943e-8, 0.0], 
                    [2.28098e-09, 1.37508e-09, 1.42559e-09, 4.943e-8, 0.0], 
                    [2.28098e-09, 1.37508e-09, 1.42559e-09, 4.943e-8, 0.0]
                ]

        
        elif shotn >= 39796:  # After October 2021 intervention
            
            # Daniel -------------------------------------------------------------------------------
            if origin == 'Daniel':
            
                A = [
                        [5.74783e-10, 9.89342e-10, 6.26369e-10, -6.67285e-08 - 1.2515e-9, 0.0], 
                        [5.74783e-10, 9.89342e-10, 6.26369e-10, -6.67285e-08 - 1.2515e-9, 0.0], 
                        [5.74783e-10, 9.89342e-10, 6.26369e-10, -6.67285e-08 - 1.2515e-9, 0.0], 
                        [5.74783e-10, 9.89342e-10, 6.26369e-10, -6.67285e-08 - 1.2515e-9, 0.0], 
                        [5.74783e-10, 9.89342e-10, 6.26369e-10, -6.67285e-08 - 1.2515e-9, 0.0]
                    ]
                B = [
                        [2.28641e-09, 1.30214e-09, 1.67195e-09, 4.94587e-08, 0.0], 
                        [2.28641e-09, 1.30214e-09, 1.67195e-09, 4.94587e-08, 0.0], 
                        [2.28641e-09, 1.30214e-09, 1.67195e-09, 4.94587e-08, 0.0], 
                        [2.28641e-09, 1.30214e-09, 1.67195e-09, 4.94587e-08, 0.0], 
                        [2.28641e-09, 1.30214e-09, 1.67195e-09, 4.94587e-08, 0.0]
                    ]
            
            # Jorge -------------------------------------------------------------------------------
            elif origin == 'Jorge':
                
                A = [
                        [6.42142000E-10, 7.28676000E-10, 9.66290e-10, -4.89265e-08-1.2515e-9, 0.0], 
                        [6.42142000E-10, 7.28676000E-10, 9.66290e-10, -4.89265e-08-1.2515e-9, 0.0], 
                        [6.64527000E-10, 8.61002000E-10, 7.99709e-10,  -4.89265e-08-1.2515e-9, 0.0], 
                        [6.95444000E-10, 9.38501000E-10, 7.53232e-10,  -4.89265e-08-1.2515e-9, 0.0], 
                        [8.98831e-10, 1.05362e-09, 7.92435e-10, -4.89265e-08-1.2515e-9, 0.0]
                ]
                B = [
                        [2.21814000E-09, 1.49154000E-09, 1.39323e-09, 5.03135e-08, 0.0], 
                        [2.21814000E-09, 1.49154000E-09, 1.39323e-09, 5.03135e-08, 0.0], 
                        [2.22729000E-09, 1.40589000E-09, 1.53190e-09, 5.03135e-08, 0.0], 
                        [2.20060000E-09, 1.35014000E-09, 1.57097e-09, 5.03135e-08, 0.0], 
                        [2.04769e-09, 1.26724e-09, 1.53654e-09, 5.03135e-08, 0.0]
                ]

            
        else:
            
        
            A = [
                    [6.42142000E-10, 7.28676000E-10, 8.69193000E-10, 0.0, 0.0], 
                    [6.42142000E-10, 7.28676000E-10, 8.69193000E-10, 0.0, 0.0], 
                    [6.64527000E-10, 8.61002000E-10, 7.84855000E-10, 0.0, 0.0], 
                    [6.95444000E-10, 9.38501000E-10, 8.51476000E-10, 0.0, 0.0], 
                    [8.98831000e-10, 1.05362000e-09, 1.09397000e-09, 0.0, 0.0]
                ]
            B = [
                    [2.21814000E-09, 1.49154000E-09, 1.46732000E-09, 0.0, 0.0], 
                    [2.21814000E-09, 1.49154000E-09, 1.46732000E-09, 0.0, 0.0], 
                    [2.22729000E-09, 1.40589000E-09, 1.53747000E-09, 0.0, 0.0], 
                    [2.20060000E-09, 1.35014000E-09, 1.48561000E-09, 0.0, 0.0], 
                    [2.04769000e-09, 1.26724000e-09, 1.29430000e-09, 0.0, 0.0]
                ]
    
    elif side == 'LFS':
        
 
        if shotn >= 41846: # After 2024 restart with new upper divertor (only V band differs from previous)
            
            A = [                # Ka updated
                    [8.12521e-10, 2.62716e-09, 8.97528e-10, -5.45257e-8, 0.0], 
                    [8.12521e-10, 2.62716e-09, 8.97528e-10, -5.45257e-8, 0.0], 
                    [8.12521e-10, 2.62716e-09, 8.97528e-10, -5.45257e-8, 0.0], 
                    [8.12521e-10, 2.62716e-09, 8.97528e-10, -5.45257e-8, 0.0], 
                    [8.12521e-10, 2.62716e-09, 8.97528e-10, -5.45257e-8, 0.0]
                ]
            B = [                # Ka updated
                    [1.89438e-10, 2.15172e-09, 2.21548e-10, 3.6731e-8, 0.0], 
                    [1.89438e-10, 2.15172e-09, 2.21548e-10, 3.6731e-8, 0.0], 
                    [1.89438e-10, 2.15172e-09, 2.21548e-10, 3.6731e-8, 0.0], 
                    [1.89438e-10, 2.15172e-09, 2.21548e-10, 3.6731e-8, 0.0], 
                    [1.89438e-10, 2.15172e-09, 2.21548e-10, 3.6731e-8, 0.0]
                ]
        
        elif shotn >= 39796:  # After October 2021 intervention
            
            # Daniel -----------------------------------------------------------------------------------
            if origin == 'Daniel':
        

                # Recalculated in 19.Sep.2023
                # A = [
                #         [9.28980e-10, 2.82448e-09, 9.66616e-10, -6.30925e-08 + 0.1294e-9, 0.0], 
                #         [9.28980e-10, 2.82448e-09, 9.66616e-10, -6.30925e-08 + 0.1294e-9, 0.0], 
                #         [9.28980e-10, 2.82448e-09, 9.66616e-10, -6.30925e-08 + 0.1294e-9, 0.0], 
                #         [9.28980e-10, 2.82448e-09, 9.66616e-10, -6.30925e-08 + 0.1294e-9, 0.0],
                #         [9.28980e-10, 2.82448e-09, 9.66616e-10, -6.30925e-08 + 0.1294e-9, 0.0]
                #     ]
                # B = [
                #         [1.19480e-10, 2.01155e-09, 1.82104e-10, 4.46267e-08, 0.0], 
                #         [1.19480e-10, 2.01155e-09, 1.82104e-10, 4.46267e-08, 0.0], 
                #         [1.19480e-10, 2.01155e-09, 1.82104e-10, 4.46267e-08, 0.0], 
                #         [1.19480e-10, 2.01155e-09, 1.82104e-10, 4.46267e-08, 0.0], 
                #         [1.19480e-10, 2.01155e-09, 1.82104e-10, 4.46267e-08, 0.0]
                #     ]

                A = [
                        [8.67515e-10, 2.63571e-09, 9.13336e-10, -6.30925e-08 + 0.1294e-9, 0.0], 
                        [8.67515e-10, 2.63571e-09, 9.13336e-10, -6.30925e-08 + 0.1294e-9, 0.0], 
                        [8.67515e-10, 2.63571e-09, 9.13336e-10, -6.30925e-08 + 0.1294e-9, 0.0], 
                        [8.67515e-10, 2.63571e-09, 9.13336e-10, -6.30925e-08 + 0.1294e-9, 0.0],
                        [8.67515e-10, 2.63571e-09, 9.13336e-10, -6.30925e-08 + 0.1294e-9, 0.0]
                    ]
                B = [
                        [1.65980e-10, 2.14409e-09, 2.20305e-10, 4.46267e-08, 0.0], 
                        [1.65980e-10, 2.14409e-09, 2.20305e-10, 4.46267e-08, 0.0], 
                        [1.65980e-10, 2.14409e-09, 2.20305e-10, 4.46267e-08, 0.0], 
                        [1.65980e-10, 2.14409e-09, 2.20305e-10, 4.46267e-08, 0.0], 
                        [1.65980e-10, 2.14409e-09, 2.20305e-10, 4.46267e-08, 0.0]
                    ]
            
            # Jorge ------------------------------------------------------------------------------------
            if origin == 'Jorge':
                A = [
                        [3.93868000E-10, 2.40418e-09, 8.28270e-10, -3.63758e-08, 0.0], 
                        [3.93868000E-10, 2.40418e-09, 8.28270e-10, -3.60590e-08, 0.0], 
                        [6.13328000E-10, 2.56171e-09, 8.28270e-10, -3.55787e-08, 0.0], 
                        [6.95494000E-10, 2.83633e-09, 8.64487e-10, -3.55787e-08, 0.0], 
                        [7.09084e-10, 2.83633e-09, 8.57062e-10, -3.55787e-08, 0.0]
                    ]
                B = [
                        [4.93676000E-10, 2.29997e-09, 2.77007e-10,  3.68210e-08, 0.0], 
                        [4.93676000E-10, 2.29997e-09, 2.77007e-10,  3.65633e-08, 0.0], 
                        [3.58072000E-10, 2.18893e-09, 2.77007e-10,  3.61894e-08, 0.0], 
                        [2.97048000E-10, 1.98991e-09, 2.56316e-10,  3.61894e-08, 0.0], 
                        [2.86316e-10, 1.98991e-09, 2.64731e-10,  3.61894e-08, 0.0]
                    ]
        
        
        # Shots before and after 36800 require different treatment on the LFS
        elif shotn >= 36800: 
            
            A = [
                    [3.93868000E-10, 2.705000e-09, 1.1297e-09, -4.36193e-08, 0.0], 
                    [3.93868000E-10, 2.545120e-09, 1.1297e-09, -4.36193e-08, 0.0], 
                    [6.13328000E-10, -2.86456e-08, 1.1297e-09, -4.36193e-08, 0.0], 
                    [6.95494000E-10, 2.767080e-09, 1.1297e-09, -4.36193e-08, 0.0],
                    [7.09084000e-10, 2.705000e-09, 1.1297e-09, -4.36193e-08, 0.0]
                ]
            B = [
                    [4.93676000E-10, 0.000000000, 0.0, 3.65431e-08, 0.0], 
                    [4.93676000E-10, 2.19249e-09, 0.0, 3.65431e-08, 0.0], 
                    [3.58072000E-10, 0.000000000, 0.0, 3.65431e-08, 0.0], 
                    [2.97048000E-10, 2.04084e-09, 0.0, 3.65431e-08, 0.0], 
                    [2.86316000e-10, 0.000000000, 0.0, 3.65431e-08, 0.0]
                ]
        
        else:
            A = [
                    [3.93868000E-10, 2.705e-09, 1.1297e-09, 0.0, 0.0], 
                    [6.13328000E-10, 2.705e-09, 1.1297e-09, 0.0, 0.0], 
                    [6.95494000E-10, 2.705e-09, 1.1297e-09, 0.0, 0.0],
                    [7.09084000e-10, 2.705e-09, 1.1297e-09, 0.0, 0.0],
                    [0.000000000000, 0.0000000, 0.00000000, 0.0, 0.0],
                
                ]
            B = [
                    [4.93676000E-10, 0.0, 0.0, 0.0, 0.0], 
                    [3.58072000E-10, 0.0, 0.0, 0.0, 0.0], 
                    [2.97048000E-10, 0.0, 0.0, 0.0, 0.0], 
                    [2.86316000e-10, 0.0, 0.0, 0.0, 0.0],
                    [0.000000000000, 0.0, 0.0, 0.0, 0.0],
                ]
            
    else:
        raise ValueError("argument `side` must be either 'LFS' for low-field side or 'HFS' for high-field side")
        
    a = A[ws_index][band_index]
    b = B[ws_index][band_index]

    if verbose:
        print(f"A: {a}\nB: {b}")
    
    return a, b


def get_dispersion_phase(shot, band, side, f1, r, t):
    """Get the dispersion phase correction for a given shot, band and side

    Parameters
    ----------
    shot: int
        Shot number
    band: str {'K', 'Ka', 'Q', 'V', 'W'}
        Desired microwave band
    side: str {'HFS', 'LFS'}
        High-field side (HFS) or low-field side (LFS)
    f1: float
        Starting frequency of the signal (Hz)
    r: float
        Frequency sweep rate of the signal (Hz/s)
    t: ndarray
        Time array of the signal (s)

    Returns
    -------
    correction: ndarray
        Complex array with the phase correction for the dispersion

    Examples
    --------
    >>> correction = get_dispersion_phase(41846, 'V', 'HFS')
    >>> corrected_signal = signal - (2**11 + 1j * 2**11)  # Center around zero
    >>> corrected_signal = corrected_signal * correction  # Multiply by dispersion phase
    """
    c = 299792458.0  # light speed in m.s^-1

    A, B = get_A_B(shot, band, side, ws='256')
    fc = get_band_cutoff_frequency(band) * 1e9

    f = f1 + r * t
    phase = r * (A * t + B * f * (1 - (fc / f) ** 2) ** 0.5 / r)

    correction = np.exp(-1.0j * 2 * np.pi * phase)

    # Correct for antenna offset
    antenna_offset = np.abs(get_antenna_position(side, antenna=band) - get_antenna_position(side, antenna='frontmost'))
    additional_delay = 2 * antenna_offset / c
    beat_frequency_offset = r * additional_delay
    correction *= np.exp(-1.0j * 2 * np.pi * beat_frequency_offset * t)

    return correction
