import numpy as np


def column_wise_max_with_quadratic_interpolation(array, column_coordinate=None):
    """
    Compute the estimated location and value of the maximum for each column 
    in a 2D array using quadratic interpolation.

    This function identifies the column-wise maximum values in a given 2D array 
    and refines the location of these maxima by fitting a parabola through the 
    maximum and its neighboring points. The estimated peak location and value 
    are then computed using this quadratic interpolation.

    Parameters
    ----------
    array : np.ndarray
        A 2D NumPy array where the maximum values and their locations are to be 
        computed for each column.
    column_coordinate : np.ndarray, optional
        A 1D array specifying the coordinates along which the maxima are to be 
        interpolated. If provided, it should have the same length as the number 
        of rows in `array`. The estimated maxima locations will be transformed 
        into these coordinates.

    Returns
    -------
    estimate_maximizer : np.ndarray
        A 1D array of the estimated indices (fractional) of the maxima for each 
        column, refined by quadratic interpolation. If `column_coordinate` is 
        provided, these indices will correspond to the interpolated coordinates.
    
    estimate_maximum : np.ndarray
        A 1D array of the estimated maximum values for each column after quadratic 
        interpolation.

    Notes
    -----
    - The quadratic interpolation assumes a parabolic shape around the maximum 
      and uses the neighboring points to estimate the peak location more accurately.
    - Edge cases where the maximum occurs at the boundaries are handled by reflecting 
      the indices to avoid out-of-bounds errors.
    - If `column_coordinate` is provided, the resulting maxima locations are mapped 
      to the specified coordinate system, allowing for a more meaningful interpretation 
      of the results.
    """

    # Indices of the maxima
    max_idx = np.argmax(array, axis=0)
    
    # Indices of the neighbours, handle edge cases by reflection
    left_idx = max_idx - 1
    left_idx[left_idx == -1] = 1
    right_idx = max_idx + 1
    right_idx[right_idx == array.shape[0]] = array.shape[0] - 2

    # Get the max amplitude and its neighbours
    max_amplitude = np.take_along_axis(array, max_idx[None, :], axis=0)[0, :]
    left_amplitude = np.take_along_axis(array, left_idx[None, :], axis=0)[0, :]
    right_amplitude = np.take_along_axis(array, right_idx[None, :], axis=0)[0, :]

    # Compute the peak coordinates using a parabola
    def quadratic_peak_finding(a, b, c):
        p = 0.5 * (a - c) / (a - 2 * b + c)
        y_p = b - 0.25 * (a - c) * p
        return p, y_p

    p, estimate_maximum = quadratic_peak_finding(left_amplitude, max_amplitude, right_amplitude)
    estimate_maximizer = max_idx + p
    
    if column_coordinate is not None:
        if len(column_coordinate) != array.shape[0]:
            raise ValueError("The length of the column coordinate should be the same as the array shape.")
        estimate_maximizer = np.interp(estimate_maximizer, np.arange(len(column_coordinate)), column_coordinate)

    return estimate_maximizer, estimate_maximum