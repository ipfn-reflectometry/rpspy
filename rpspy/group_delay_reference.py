import aug_sfutils as sf
import numpy as np
import sys


def get_group_delay_reference(shot, experiment='AUGD', edition=0):
    """Returns the group delay reference (major radius for group delay zero) for a given RPS shotfile.
    
    Parameters
    -----------
    shot: int
        Number of the discharge
    experiment: str, optional
        Experiment where to read the data. For private shotfiles, it is the username of the owner. Default is 'AUGD', the public shotfile.
    edition: int, optional
        Version of the shotfile to be opened. Defaults to 0, the latest available edition.
        
    Returns
    -------
    R_HFS, R_LFS: float
        Major radius on the HFS/LFS corresponding to zero group delay.    
    """
    
    c = 299792458.0

    # Open RPS shotfile
    if (edition == 0) and (experiment == 'AUGD'):
        shotfile = sf.SFREAD(shot, 'RPS')
    else:
        shotfile = sf.SFREAD(sf=sf.manage_ed.sf_path(shot, 'RPS', exp=experiment, ed=edition)[0])
   
    # Get HFS & LFS signals
    gd_hfs = shotfile.getobject('GD_HFS')
    r_hfs  = shotfile.getobject('RB_HFS')
    
    gd_lfs = shotfile.getobject('GD_LFS')
    r_lfs  = shotfile.getobject('RB_LFS')

    # Find index of first finite element
    idx_hfs = np.argmax(np.isfinite(gd_hfs[:, 0]))
    idx_lfs = np.argmax(np.isfinite(gd_lfs[:, 0]))
    
    # Calculate major radius at zero group delay
    reference_hfs = r_hfs[0, idx_hfs] - gd_hfs[idx_hfs, 0] * c / 2
    reference_lfs = r_lfs[0, idx_lfs] + gd_lfs[idx_lfs, 0] * c / 2    
    
    return reference_hfs, reference_lfs


if __name__ == "__main__":
    hfs, lfs = get_group_delay_reference(int(sys.argv[1]))
    print(f"Major radius corresponding to zero group delay is:\nHFS: {hfs} m\nLFS: {lfs} m")