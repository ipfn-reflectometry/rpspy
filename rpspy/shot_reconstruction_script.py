import rpspy

from typing import Union

import typer
from typing_extensions import Annotated

app = typer.Typer(rich_markup_mode="rich")


### Function to be wrapped...

# def full_profile_reconstruction(
#     shot: int, 
#     destination_dir: str = '.', 
#     shotfile_dir=None, 
#     linearization_shotfile_dir=None, 
#     sweep_linearization=None, 
#     shot_linearization=None,
#     spectrogram_options=None, 
#     filters=None,
#     subtract_on_bands=None,
#     start_time: float = 0.0, 
#     end_time: float = 10.0, 
#     time_step: float = 1e-3,
#     burst: int = 29, 
#     write_dump: bool = True, 
#     return_profiles: bool = False
# ):
#     """
#     Performs full profile reconstruction for a given shot, with results optionally saved to a dump file.
#     This process involves analyzing spectrograms, applying filters, and reconstructing
#     density profiles over a specified time range.

#     Parameters
#     ----------
#     shot : int
#         Shot number identifying the experiment or simulation run.
#     destination_dir : str, optional
#         Directory to save the output dump file. Default is the current directory ('.').
#     shotfile_dir : str, optional
#         Directory containing the shot files. Default is None, indicating use of default location.
#     linearization_shotfile_dir : str, optional
#         Directory for linearization shot files. Default is None, indicating use of default location.
#     sweep_linearization : int, optional
#         Sweep number for linearization. Default is obtained internally.
#     shot_linearization : int, optional
#         Shot number for shot linearization. Default is obtained internally.
#     spectrogram_options : dict, optional
#         Configuration options for generating spectrograms. Default is:
#         {
#             'K': {'nperseg': 128, 'noverlap':96, 'nfft': 1024},
#             'Ka': {'nperseg': 128, 'noverlap':96, 'nfft': 1024},
#             'Q': {'nperseg': 128, 'noverlap':96, 'nfft': 1024},
#             'V': {'nperseg': 32, 'noverlap':24, 'nfft': 1024},
#         }
#     filters : dict, optional
#         Dictionary of filters to apply during reconstruction. Default is:
#         {
#             'HFS': {
#                 'K': [0.0e6, 4.0e6],
#                 'Ka': [0.0e6, 4.0e6],
#                 'Q': [0.0e6, 4.0e6],
#                 'V': [0.0e6, 6.0e6],
#             },
#             'LFS': {
#                 'K': [0.2e6, 4.0e6],
#                 'Ka': [0.0e6, 4.0e6],
#                 'Q': [0.0e6, 4.0e6],
#                 'V': [0.8e6, 6.0e6],
#             },
#         }.
#     subtract_on_bands: list, optional
#         List of '<band>-<side>' to subtract background. Example ['Q-LFS', 'Q-HFS']. 
#     start_time : float, optional
#         Start time of the reconstruction in seconds. Default is 0.0.
#     end_time : float, optional
#         End time of the reconstruction in seconds. Default is 10.0.
#     time_step : float, optional
#         Time step for the reconstruction in seconds. Default is 1e-3.
#     burst : int, optional
#         Burst parameter for controlling the reconstruction process. Default is 29.
#     write_dump : bool, optional
#         If True, writes the reconstructed data to a dump file using the specified architecture. Default is True.
#     return_profiles : bool, optional
#         If True, returns the processed density profiles. Default is False.

#     Returns
#     -------
#     tuple or None
#         If `return_profiles` is True, returns a tuple containing:
        
#         - time (ndarray): Time instants corresponding to the processed density profiles.
#         - density (ndarray): Plasma density profile.
#         - r_hfs (ndarray): Radial position of each density layer on the high-field side.
#         - r_lfs (ndarray): Radial position of each density layer on the low-field side.
        
#         If `return_profiles` is False, returns None.
#     """

#     ### Function body...

#     pass


### Typer example...

# @app.command()
# def create(
#     username: Annotated[str, typer.Argument(help="The username to create")],
#     lastname: Annotated[
#         str,
#         typer.Argument(
#             help="The last name of the new user", rich_help_panel="Secondary Arguments"
#         ),
#     ] = "",
#     force: Annotated[bool, typer.Option(help="Force the creation of the user")] = False,
#     age: Annotated[
#         Union[int, None],
#         typer.Option(help="The age of the new user", rich_help_panel="Additional Data"),
#     ] = None,
#     favorite_color: Annotated[
#         Union[str, None],
#         typer.Option(
#             help="The favorite color of the new user",
#             rich_help_panel="Additional Data",
#         ),
#     ] = None,
# ):
#     """
#     [green]Create[/green] a new user. :sparkles:
#     """
#     print(f"Creating user: {username}")



@app.command()
def full_shot_reconstruction_cli_wrapper(
    shot: Annotated[int, typer.Argument(help="Shot number.")],
    destination_dir: Annotated[str, typer.Option(
        help="Directory to save the output dump file.",
        rich_help_panel="Secondary Arguments"),
        ] = '.',
    shotfile_dir: Annotated[Union[str, None], typer.Option(
        help="Directory containing the shot files.",
        rich_help_panel="Secondary Arguments"),
        ] = None,
    linearization_shotfile_dir: Annotated[Union[str, None], typer.Option(
        help="Directory for linearization shot files.",
        rich_help_panel="Secondary Arguments"),
        ] = None,
    sweep_linearization: Annotated[Union[int, None], typer.Option(
        help="Sweep number for linearization.",
        rich_help_panel="Secondary Arguments"),
        ] = None,
    shot_linearization: Annotated[Union[int, None], typer.Option(
        help="Shot number for shot linearization.",
        rich_help_panel="Secondary Arguments"),
        ] = None,
    k_nperseg: Annotated[int, typer.Option(
        help="Number of data points used in each block for the FFT.",
        rich_help_panel="Spectrogram Options"),
        ] = 128,
    k_noverlap: Annotated[int, typer.Option(
        help="Number of points of overlap between blocks.",
        rich_help_panel="Spectrogram Options"),
        ] = 96,
    k_nfft: Annotated[int, typer.Option(
        help="Length of the FFT used.",
        rich_help_panel="Spectrogram Options"),
        ] = 1024,
    ka_nperseg: Annotated[int, typer.Option(
        help="Number of data points used in each block for the FFT.",
        rich_help_panel="Spectrogram Options"),
        ] = 128,
    ka_noverlap: Annotated[int, typer.Option(
        help="Number of points of overlap between blocks.",
        rich_help_panel="Spectrogram Options"),
        ] = 96,
    ka_nfft: Annotated[int, typer.Option(
        help="Length of the FFT used.",
        rich_help_panel="Spectrogram Options"),
        ] = 1024,
    q_nperseg: Annotated[int, typer.Option(
        help="Number of data points used in each block for the FFT.",
        rich_help_panel="Spectrogram Options"),
        ] = 128,
    q_noverlap: Annotated[int, typer.Option(
        help="Number of points of overlap between blocks.",
        rich_help_panel="Spectrogram Options"),
        ] = 96,
    q_nfft: Annotated[int, typer.Option(
        help="Length of the FFT used.",
        rich_help_panel="Spectrogram Options"),
        ] = 1024,
    v_nperseg: Annotated[int, typer.Option(
        help="Number of data points used in each block for the FFT.",
        rich_help_panel="Spectrogram Options"),
        ] = 32,
    v_noverlap: Annotated[int, typer.Option(
        help="Number of points of overlap between blocks.",
        rich_help_panel="Spectrogram Options"),
        ] = 24,
    v_nfft: Annotated[int, typer.Option(
        help="Length of the FFT used.",
        rich_help_panel="Spectrogram Options"),
        ] = 1024,
    hfs_k_min: Annotated[float, typer.Option(
        help="Minimum frequency for the HFS K-band filter.",
        rich_help_panel="Filters"),
        ] = 0.0e6,
    hfs_k_max: Annotated[float, typer.Option(
        help="Maximum frequency for the HFS K-band filter.",
        rich_help_panel="Filters"),
        ] = 4.0e6,
    hfs_ka_min: Annotated[float, typer.Option(
        help="Minimum frequency for the HFS Ka-band filter.",
        rich_help_panel="Filters"),
        ] = 0.0e6,
    hfs_ka_max: Annotated[float, typer.Option(
        help="Maximum frequency for the HFS Ka-band filter.",
        rich_help_panel="Filters"),
        ] = 4.0e6,
    hfs_q_min: Annotated[float, typer.Option(
        help="Minimum frequency for the HFS Q-band filter.",
        rich_help_panel="Filters"),
        ] = 0.0e6,
    hfs_q_max: Annotated[float, typer.Option(
        help="Maximum frequency for the HFS Q-band filter.",
        rich_help_panel="Filters"),
        ] = 4.0e6,
    hfs_v_min: Annotated[float, typer.Option(
        help="Minimum frequency for the HFS V-band filter.",
        rich_help_panel="Filters"),
        ] = 0.0e6,
    hfs_v_max: Annotated[float, typer.Option(
        help="Maximum frequency for the HFS V-band filter.",
        rich_help_panel="Filters"),
        ] = 6.0e6,
    lfs_k_min: Annotated[float, typer.Option(
        help="Minimum frequency for the LFS K-band filter.",
        rich_help_panel="Filters"),
        ] = 0.2e6,
    lfs_k_max: Annotated[float, typer.Option(
        help="Maximum frequency for the LFS K-band filter.",
        rich_help_panel="Filters"),
        ] = 4.0e6,
    lfs_ka_min: Annotated[float, typer.Option(
        help="Minimum frequency for the LFS Ka-band filter.",
        rich_help_panel="Filters"),
        ] = 0.0e6,
    lfs_ka_max: Annotated[float, typer.Option(
        help="Maximum frequency for the LFS Ka-band filter.",
        rich_help_panel="Filters"),
        ] = 4.0e6,
    lfs_q_min: Annotated[float, typer.Option(
        help="Minimum frequency for the LFS Q-band filter.",
        rich_help_panel="Filters"),
        ] = 0.0e6,
    lfs_q_max: Annotated[float, typer.Option(
        help="Maximum frequency for the LFS Q-band filter.",
        rich_help_panel="Filters"),
        ] = 4.0e6,
    lfs_v_min: Annotated[float, typer.Option(
        help="Minimum frequency for the LFS V-band filter.",
        rich_help_panel="Filters"),
        ] = 0.8e6,
    lfs_v_max: Annotated[float, typer.Option(
        help="Maximum frequency for the LFS V-band filter.",
        rich_help_panel="Filters"),
        ] = 6.0e6,
    k_hfs_background_subtract: Annotated[bool, typer.Option(
        help="Subtract background for the K-band HFS.",
        rich_help_panel="Filters"),
        ] = False,
    ka_hfs_background_subtract: Annotated[bool, typer.Option(
        help="Subtract background for the Ka-band HFS.",
        rich_help_panel="Filters"),
        ] = False,
    q_hfs_background_subtract: Annotated[bool, typer.Option(
        help="Subtract background for the Q-band HFS.",
        rich_help_panel="Filters"),
        ] = False,
    v_hfs_background_subtract: Annotated[bool, typer.Option(
        help="Subtract background for the V-band HFS.",
        rich_help_panel="Filters"),
        ] = False,
    k_lfs_background_subtract: Annotated[bool, typer.Option(
        help="Subtract background for the K-band LFS.",
        rich_help_panel="Filters"),
        ] = False,
    ka_lfs_background_subtract: Annotated[bool, typer.Option(
        help="Subtract background for the Ka-band LFS.",
        rich_help_panel="Filters"),
        ] = False,
    q_lfs_background_subtract: Annotated[bool, typer.Option(
        help="Subtract background for the Q-band LFS.",
        rich_help_panel="Filters"),
        ] = False,
    v_lfs_background_subtract: Annotated[bool, typer.Option(
        help="Subtract background for the V-band LFS.",
        rich_help_panel="Filters"),
        ] = False,
    v_hfs_dispersion_subtract: Annotated[bool, typer.Option(
        help="Subtract dispersion to the V-band HFS IQ signal.",
        rich_help_panel="Filters"),
        ] = False,
    v_lfs_dispersion_subtract: Annotated[bool, typer.Option(
        help="Subtract dispersion to the V-band LFS IQ signal.",
        rich_help_panel="Filters"),
        ] = False,
    start_time: Annotated[float, typer.Option(
        help="Start time of the reconstruction in seconds.",
        rich_help_panel="Reconstruction Options"),
        ] = 0.0,
    end_time: Annotated[float, typer.Option(
        help="End time of the reconstruction in seconds.",
        rich_help_panel="Reconstruction Options"),
        ] = 10.0,
    time_step: Annotated[float, typer.Option(
        help="Time step for the reconstruction in seconds.",
        rich_help_panel="Reconstruction Options"),
        ] = 1e-3,
    burst: Annotated[int, typer.Option(
        help="Burst parameter for controlling the reconstruction process.",
        rich_help_panel="Reconstruction Options"),
        ] = 29,
    write_dump: Annotated[bool, typer.Option(
        help="If True, writes the reconstructed data to a dump file using the specified architecture.",
        rich_help_panel="Reconstruction Options"),
        ] = True,
    
   
):
    """
    Performs full profile reconstruction for a given shot, with results optionally saved to a dump file.
    """
    # Parse JSON strings to dictionaries
  
    spectrogram_options_dict = {
        'K': {'nperseg': k_nperseg, 'noverlap': k_noverlap, 'nfft': k_nfft},
        'Ka': {'nperseg': ka_nperseg, 'noverlap': ka_noverlap, 'nfft': ka_nfft},
        'Q': {'nperseg': q_nperseg, 'noverlap': q_noverlap, 'nfft': q_nfft},
        'V': {'nperseg': v_nperseg, 'noverlap': v_noverlap, 'nfft': v_nfft},
    }

    filters_dict = {
        'HFS': {
            'K': [hfs_k_min, hfs_k_max],
            'Ka': [hfs_ka_min, hfs_ka_max],
            'Q': [hfs_q_min, hfs_q_max],
            'V': [hfs_v_min, hfs_v_max],
        },
        'LFS': {
            'K': [lfs_k_min, lfs_k_max],
            'Ka': [lfs_ka_min, lfs_ka_max],
            'Q': [lfs_q_min, lfs_q_max],
            'V': [lfs_v_min, lfs_v_max],
        },
    }

    subtract_background_on_bands = []
    if k_hfs_background_subtract:
        subtract_background_on_bands.append('K-HFS')
    if ka_hfs_background_subtract:
        subtract_background_on_bands.append('Ka-HFS')
    if q_hfs_background_subtract:
        subtract_background_on_bands.append('Q-HFS')
    if v_hfs_background_subtract:
        subtract_background_on_bands.append('V-HFS')
    if k_lfs_background_subtract:
        subtract_background_on_bands.append('K-LFS')
    if ka_lfs_background_subtract:
        subtract_background_on_bands.append('Ka-LFS')
    if q_lfs_background_subtract:
        subtract_background_on_bands.append('Q-LFS')
    if v_lfs_background_subtract:
        subtract_background_on_bands.append('V-LFS')

    subtract_dispersion_on_bands = []
    if v_hfs_dispersion_subtract:
        subtract_dispersion_on_bands.append('V-HFS')
    if v_lfs_dispersion_subtract:
        subtract_dispersion_on_bands.append('V-LFS')

    rpspy.full_profile_reconstruction(
        shot=shot,
        destination_dir=destination_dir,
        shotfile_dir=shotfile_dir,
        linearization_shotfile_dir=linearization_shotfile_dir,
        sweep_linearization=sweep_linearization,
        shot_linearization=shot_linearization,
        spectrogram_options=spectrogram_options_dict,
        filters=filters_dict,
        subtract_background_on_bands=subtract_background_on_bands,
        subtract_dispersion_on_bands=subtract_dispersion_on_bands,
        start_time=start_time,
        end_time=end_time,
        time_step=time_step,
        burst=burst,
        write_dump=write_dump,
        return_profiles=False,
    )
    

def main():
    typer.run(full_shot_reconstruction_cli_wrapper)
    
    