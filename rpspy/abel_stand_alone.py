import numpy as np
from scipy.linalg import blas  # A is triangular matrix, use blas for that. Saves about factor 1.
from scipy.constants import c as c0  # vacuum speed of light
import functools

@functools.lru_cache(maxsize=128)
def get_A_Ainv(*freqs):
    # taugs = np.array(taugs, order='F')  # needed for BLAS?
    freqs = np.array(freqs)
    fsq = freqs*freqs  # frequency squared

    N = fsq.size-1  # size of system
#     try:  # if called consequtively, reuse matrix
#         # potential issue: dimensions or frequency vector might change between different calls.
#         if not re_use_matrix:
#             1/0
#         A = PWLD_matrix_batch.A
#         Ainv = PWLD_matrix_batch.Ainv
#         B = PWLD_matrix_batch.B
#         C = PWLD_matrix_batch.C
#     except:
    # print("Calculating new A")
    A = np.zeros([N, N], order='F')

    # 1) Build Matrix
    for i in range(N):  # for all frequencies
        # the inverse of the sum (half the inverse mean) of the refractive indices at supports
        A[i, :i+1] = 1.0 / (np.sqrt(1.0 - fsq[1:i+2] / fsq[i+1]) + np.sqrt(1.0 - fsq[0:i+1] / fsq[i+1]))
    # multiply constant: 2/c from the basic integral and another factor 2 because two way.
    A *= 4.0 / c0

    # 2) Invert matrix
#     PWLD_matrix_batch.A = A
    Ainv = np.linalg.pinv(A)  # this will provide length of intervals between supports
    
    return A, Ainv


def PWLD_matrix_batch(
        taugs, freqs,
        x0=0.0, orientation=1.0,
        feedback=1, use_blas=True,
        re_use_matrix=True,
        return_dic=True):
    """
    Assuming taugs has shape [Nfreq, Nprofiles], where Nfreq is the number of frequencies > 0.
    If frequency zero is not included, it is internally added.
    This is not strictly necessary, but an assumption to prevent confusion.
    """
    Nf_0 = freqs.size  # original system size
    if taugs.ndim == 1:  # ensure right dimensionality
        taugs = np.atleast_2d(taugs)
    if freqs[0] > 0:  # add zero
        freqs = np.concatenate(([0], freqs))
        # taugs = np.concatenate(([0], taugs))
        # if taugs_sigma is not None:
        #     taugs_sigma = np.concatenate(([0], taugs_sigma))
        if taugs.shape[0] != freqs.size - 1:
            # taugs = taugs.T * 1
            print('taugs has shape {} - must it be transposed?'.format(taugs.shape))
            print("expecting ({}, :)".format(freqs.size - 1))
            raise ValueError('shapes do not agree: fP {}, gd {}'.format(freqs.shape, taugs.shape))
    else:
        if taugs.shape[0] == freqs.size:
            # taugs = taugs.T * 1
            taugs = taugs[1:, :] * 1
        elif taugs.shape[0] == freqs.size - 1:
            pass  # already right length
        else:
            raise ValueError('taugs has shape {} - must it be transposed?'.format(taugs.shape))
        
    # Create A and A_inv in a chached function to save time
    A, Ainv = get_A_Ainv(*freqs)

#     # taugs = np.array(taugs, order='F')  # needed for BLAS?
#     fsq = freqs*freqs  # frequency squared

#     N = fsq.size-1  # size of system
# #     try:  # if called consequtively, reuse matrix
# #         # potential issue: dimensions or frequency vector might change between different calls.
# #         if not re_use_matrix:
# #             1/0
# #         A = PWLD_matrix_batch.A
# #         Ainv = PWLD_matrix_batch.Ainv
# #         B = PWLD_matrix_batch.B
# #         C = PWLD_matrix_batch.C
# #     except:
#     # print("Calculating new A")
#     A = np.zeros([N, N], order='F')

#     # 1) Build Matrix
#     for i in range(N):  # for all frequencies
#         # the inverse of the sum (half the inverse mean) of the refractive indices at supports
#         A[i, :i+1] = 1.0 / (np.sqrt(1.0 - fsq[1:i+2] / fsq[i+1]) + np.sqrt(1.0 - fsq[0:i+1] / fsq[i+1]))
#     # multiply constant: 2/c from the basic integral and another factor 2 because two way.
#     A *= 4.0 / c0

#     # 2) Invert matrix
# #     PWLD_matrix_batch.A = A
#     Ainv = np.linalg.pinv(A)  # this will provide length of intervals between supports
#     PWLD_matrix_batch.Ainv = Ainv
    # express cumulative sum as matrix
    B = np.zeros_like(Ainv, order='F')
#     PWLD_matrix_batch.B = B
    for idx in range(B.shape[0]):
        B[idx:, idx] = 1
    B *= orientation  # if LFS, the interval length describes distance from antenna to lower R
    # matrix to directly get R vector
    C = np.matmul(B, Ainv)
#     PWLD_matrix_batch.C = C

    if return_dic:  # store all the input if wanted
        res = {
            'A': A,
            'f_P': freqs[-Nf_0:],
            'taugs': taugs
            }
        res['Ainv'] = Ainv
        res['B'] = B
        res['C'] = C

    if feedback >= 2:  # 2A) print condition number
        cond = np.linalg.cond(A)
        print("A condition number is {:.5e}".format(cond))

    # 3) Apply to data
    if use_blas:  # use_blas:
        # use double triangular matrix mulatiplication from MKL's BLAS - fast and evaluates only what needed
        # (math kernel library provides basic linear algebra subprograms)
        R = blas.dtrmm(a=C, b=taugs, alpha=1, lower=1)
        # d = double precision
        # tr = triangular
        # mm = matrix multiplication
    else:
        R = np.matmul(C, taugs)  # zero-frequency group delay not relevant

    R += x0
    if not return_dic:
        return R
    else:
        res['R'] = R
        return res

if __name__ == '__main__':
    freq_vec = np.arange(10) * 5e9
    taug_mat = np.atleast_2d(np.arange(10)).T * 0.1e-9
    if 0:  # exlcude zero
        freq_vec = freq_vec[1:]  # ignore first frequency
        taug_mat = taug_mat[1:, :]

    print("frequency vector shape: {}, first element {:.2e}".format(freq_vec.shape, freq_vec[0]))
    print("group del matrix shape: {}".format(taug_mat.shape))
    res = PWLD_matrix_batch(taug_mat, freq_vec)
    print("resulting matrix shape: {}".format(res['R'].shape))

