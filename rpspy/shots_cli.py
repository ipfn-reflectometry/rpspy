import typer
from pathlib import Path
from typing import Optional
import aug_sfutils as sf
from rpspy import check_rawdata

# External functions for interaction with the database
def get_last_shot_number() -> int:
    """Returns the last shot number available in the experiment database."""
    jou = sf.JOURNAL()
    try:
        nshot = jou.getLastShot()  # Retrieve the last shot number
        return nshot
    finally:
        jou.close()

def shot_number_exists(shot: int) -> bool:
    """Checks if the given shot number exists in the database."""
    return check_rawdata(shot) and sf.JOURNAL().isUseful(shot)

DATA_FILE = Path("shots_data.txt")

app = typer.Typer()

def load_data_file():
    """Loads the existing shots data from the file."""
    if DATA_FILE.exists():
        with DATA_FILE.open("r") as file:
            lines = file.readlines()
            last_saved_shot = int(lines[0].strip())
            existing_shots = set(map(int, lines[1:]))
            return last_saved_shot, existing_shots
    return 0, set()

def update_data_file(last_saved_shot: int, existing_shots: set):
    """Updates the shots data file with the current information."""
    with DATA_FILE.open("w") as file:
        file.write(f"{last_saved_shot}\n")
        file.writelines(f"{shot}\n" for shot in sorted(existing_shots))

def sync_shots():
    """Synchronizes the data file with the latest shots information."""
    last_saved_shot, existing_shots = load_data_file()
    current_last_shot = get_last_shot_number()

    if current_last_shot > last_saved_shot:
        for shot in range(last_saved_shot + 1, current_last_shot + 1):
            if shot_number_exists(shot):
                existing_shots.add(shot)

        update_data_file(current_last_shot, existing_shots)

    return existing_shots

@app.command()
def list(
    all: bool = typer.Option(False, "--all", help="List all existing shots."),
    greater_than: Optional[int] = typer.Option(None, "--greater-than", help="List shots greater than this number."),
    less_than: Optional[int] = typer.Option(None, "--less-than", help="List shots less than this number."),
):
    """List existing shots based on the specified criteria."""
    existing_shots = sync_shots()

    if all:
        filtered_shots = existing_shots
    else:
        filtered_shots = existing_shots
        if greater_than is not None:
            filtered_shots = {shot for shot in filtered_shots if shot > greater_than}
        if less_than is not None:
            filtered_shots = {shot for shot in filtered_shots if shot < less_than}

    for shot in sorted(filtered_shots):
        typer.echo(shot)

if __name__ == "__main__":
    app()
