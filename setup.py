import setuptools

setuptools.setup(
    name="rpspy",
    version="0.0.0",
    url="https://gitlab.com/ipfn-reflectometry/rpspy",

    author="Daniel Hachmeister",
    author_email="daniel.hachmeister@tecnico.ulisboa.pt",

    description="Data processing functions for O-mode reflectometry",
    long_description=open('README.md').read(),
    
    entry_points={
        'console_scripts': [
            'shot-reconstruction=rpspy.shot_reconstruction_script:main',
            'shots-cli=rpspy.shots_cli:app',  # Add the CLI entry point here
        ],
    },

    packages=setuptools.find_packages(),
    install_requires=[
        "typer[all]",  # Ensure Typer is installed for the CLI functionality
    ],
)
