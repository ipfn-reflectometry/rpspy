from full_profile_reconstruction import full_profile_reconstruction
import sys


def process_several_shots(read_from, write_to):

    for shot in list(map(int, open(read_from, "r").readlines())):

        try:
            full_profile_reconstruction(shot, write_to)
        except ValueError:
            pass

    
    
if __name__ == "__main__":
    process_several_shots(sys.argv[1], sys.argv[2])    