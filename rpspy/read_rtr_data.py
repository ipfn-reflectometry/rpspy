import os
import xml.etree.ElementTree as ET
import numpy as np


def closest(array, value, dtype=None):
    """Return the index of "array" with the value closest to "value"
    Parameters
    ----------
    array: iterable
        Array of value in which to search for element closest to `value`
    value: float or iterable
        Search value. If multiple values are provided, the return is an array with the respective indexes
    dtype: np.dtype, optional
        Data type of `array`. If undefined, data type will be infered from the elements of `array`.

    Return
    ------
    idx: int, or int_ndarray
        Indexes of `array` absolutely closest to `value`

    """
    if dtype is not None:
        x = np.array(array, dtype=dtype)
    else:
        x = np.array(array)

    if hasattr(value, '__iter__'):
        if len(value) > 1:
            y = np.array(value)
            y.shape = (len(y), 1)

        else:
            y = value[0]
    else:
        y = value

    idx = np.argmin(np.abs(x-y), axis=-1)
#     idx = (np.abs(array-value)).argmin()
    return idx



def get_default_shotfile_dir(shot):
    """Returns the default shotfile directory for a given shot.
    TODO: return as a path object"""

#     return f"/afs/ipp-garching.mpg.de/home/a/augd/rawfiles/RTR/{shot//10}/{shot}/"
    return f"/shares/experiments/aug-rawfiles/RTR/{shot//10}/{shot}/"


def check_rawdata(shot: int):
    """Check if there is reflectometry raw data for a given shot number."""
    return os.path.exists(get_default_shotfile_dir(shot))


def get_sampling_frequency(shot, shotfile_dir=None):
    """Get the sampling frequency written in the .xml file relating to a given shot.
    TODO: work with path objects instead of strings

    Parameters
    ----------
    shot: int
        Shot number.
    shotfile_dir: str, optional
        Shotfile directory. Defaults to the default shofile location on AFS.

    Returns
    -------
    sampling_frequency: float
        Sampling frequency of the data acquisition in Hz.
    """

    if shotfile_dir is None:
        shotfile_dir = get_default_shotfile_dir(shot)

    tree = ET.parse(os.path.join(
        shotfile_dir, f"RTR_{shot}_AcquisitionConfiguration.xml"))
    sampling_frequency = float(
        tree.find("AcquisitionTimmingConfiguration").find("samp_freq").text) * 1e6

    return sampling_frequency


def get_markers(shot, sweep=None, shotfile_dir=None):

    if shotfile_dir is None:
        shotfile_dir = get_default_shotfile_dir(shot)

    file_name = os.path.join(shotfile_dir, f"RTR_{shot}_Markers.sig")

    if sweep is None:  # Get all markers
        markers = np.fromfile(file=file_name, dtype=np.int16, sep="")
        markers = markers.reshape((len(markers) // 1024, 1024))

    else:
        try:  # If sweeps is a tuple
            markers = np.fromfile(
                file=file_name, dtype=np.int16, sep="",
                count=1024*(sweep[1]-sweep[0]),
                offset=sweep[0]*2048)

        except TypeError:  # If sweep is an int
            markers = np.fromfile(
                file=file_name, dtype=np.int16, sep="",
                count=1024,
                offset=sweep*2048)

    return markers


def get_band_signal(shot, shotfile_dir, name, side, signal_type, sweep, number_of_sweeps=1, samples_per_sweep=1024, data_type=np.dtype('int16')):
    """Get the rawdata from the level-0 shotfile for a given band and side corresponding to one or multiple sweeps.

    Parameters
    ----------
    shot: int
        Shot number.
    shotfile_dir: str, optional
        Shotfile directory. Defaults to the default shofile location on AFS.
    name: {'K', 'Ka', 'Q', 'V'}
        Name of the microwave band
    side: {'lfs', 'hfs'}
        Low-field side or high-field side
    signal_type: {'real', 'complex'}
        Real or complexed valued data.
    sweep: int, ndarray
        Index of the data to retrieve. 
        If *sweep* is a 1D array, then all the signals are loaded from the database 
        and then the ones corresponding to *sweep* are returned. 
    number_of_sweeps: int
        Number of consecutive samples starting in *sweep*.
    samples_per_sweep: int
        Number of samples contained in each sweep of a microwave band
    data_type: np.dtype
        Instance of np.dtype specifying the data type stored in the binary file

    Returns
    -------
    signal: ndarray
        Array of data with shape (burst, 1024,)
    """

    try:  # If sweeps is iterable, read entire file then return requested indexes
        iter(sweep)
        sweep_is_iter = True
        offset = 0
        count = -1
        
    except TypeError:  # If sweep is a number, read the file starting from sweep and ending in sweep+number_of_sweeps
        sweep_is_iter = False
        offset = sweep * data_type.itemsize * samples_per_sweep
        count = number_of_sweeps * samples_per_sweep
        
    def read_and_reshape(file_name):
        x = np.fromfile(file=file_name, dtype=data_type, count=count, sep="", offset=offset)
        x = x.reshape((-1, samples_per_sweep))
        if sweep_is_iter:
            x = np.take(x, sweep, axis=0)
        return x
        
    try:
        # For complex data -------------------------------------------
        if signal_type == 'complex':
            file_name = os.path.join(
                shotfile_dir, f"RTR_{shot}_{name}q-{side.upper()}.sig")
            q_signal = read_and_reshape(file_name)            
            
            file_name = os.path.join(
                shotfile_dir, f"RTR_{shot}_{name}i-{side.upper()}.sig")
            i_signal = read_and_reshape(file_name)

            # Save the complex linearized signal -------------------------
            signal = i_signal + 1j * q_signal

        # For real data
        elif signal_type == 'real':
            file_name = os.path.join(
                shotfile_dir, f"RTR_{shot}_{name}-{side.upper()}.sig")
            signal = read_and_reshape(file_name)

        else:
            raise TypeError(
                f"{signal_type} is not a valid type. Choose between 'complex' and 'real'")

    except ValueError as e:
        raise ValueError(
            "Error while reading raw data. Probably tried reading more values than are stored.", e)

    return signal


def get_timestamps(shot, shotfile_dir=None, times=None, indices=None):
    """Get the acquisition timestamps.

    Parameters
    ----------
    shot: int
        Shot number.
    shotfile_dir: str, optional
        Shotfile directory. Defaults to the default shofile location on AFS.
    times: float or array-like
        If specified, will return the sweep indices that are closest to time.
    indices: int or array-like
        If specified, will return the timestamps corresponding to the sweeps *sweep*.

    Returns
    -------
    times or indices:
        Depending on the input, will return the entire timestamps array, the timestamps corresponding to *indices*, or the indices corresponding to *times*.

    Note:
        You can specify *times* or *indices* but not both.
    """

    if shotfile_dir is None:
        shotfile_dir = get_default_shotfile_dir(shot)

    # Read the time instants from the database ----------------------------------------------------------------
    file_name = os.path.join(shotfile_dir, f"RTR_{shot}_Sweep_double.tb")
    time_array = np.fromfile(file=file_name, dtype=np.float64, sep="")

    if (times is None) and (indices is None):
        return time_array
    elif (times is None) and (indices is not None):
        return np.take(time_array, indices)
    elif (times is not None) and (indices is None):
        return closest(time_array, times)
    else:
        raise ValueError(
            "User must provide either *times* or *indices* but not both")
